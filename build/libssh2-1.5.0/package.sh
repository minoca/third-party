##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the libssh2 library.
##
## Author:
##
##     Evan Green 2-Apr-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

if ! test -d "$BUILD_DIRECTORY/lib"; then
    echo "Not packaging, build wasn't complete. Exiting successfully anyway."
    exit 0
fi

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/lib" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/include" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libssh2
Depends: libopenssl
Priority: optional
Version: 1.5.0
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://www.libssh2.org/download/libssh2-1.5.0.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: SSH2 support library.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

