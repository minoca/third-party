##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the mbedtls (formerly PolarSSL) utility.
##
## Author:
##
##     Evan Green 16-Apr-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/lib" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/include" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libmbedtls
Priority: optional
Version: 1.3.10
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://tls.mbed.org/download/mbedtls-1.3.10-gpl.tgz
Installed-Size: $INSTALLED_SIZE
Description: mbedtls (formerly PolarSSL) library.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

