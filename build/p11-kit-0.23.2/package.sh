##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the p11 kit.
##
## Author:
##
##     Evan Green 1-Sep-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

rm "$BUILD_DIRECTORY"/usr/lib/*.la
rm "$BUILD_DIRECTORY"/usr/lib/pkcs11/*.la

mkdir -p "$PACKAGE_DIRECTORY/usr/share"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: p11kit
Depends: libtasn1, libffi, gettext, libgcc
Priority: optional
Version: 0.23.2
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://p11-glue.freedesktop.org/releases/p11-kit-0.23.2.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: PKCS#11 module kit.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

