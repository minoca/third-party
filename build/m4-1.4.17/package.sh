##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the m4 utility.
##
## Author:
##
##     Evan Green 13-Feb-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr/bin"
cp -pv "$BUILD_DIRECTORY/bin/m4" "$PACKAGE_DIRECTORY/usr/bin/m4"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: m4
Priority: optional
Version: 1.4.17
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://ftp.gnu.org/gnu/m4/m4-1.4.17.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: GNU M4 macro processor.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

