##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the pinentry tool.
##
## Author:
##
##     Evan Green 31-Aug-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: pinentry
Depends: libgpg-error, libncurses, libiconv, libassuan, libgcc
Priority: optional
Version: 0.9.7
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://gnupg.org/ftp/gcrypt/pinentry/pinentry-0.9.7.tar.bz2
Installed-Size: $INSTALLED_SIZE
Description: GNUPG pinentry tool.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

