##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the PHP package.
##
## Author:
##
##     Evan Green 16-Aug-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

NCURSES=libncurses_5.9
READLINE=libreadline_6.3
LIBZ=libz_1.2.11
LIBGMP=libgmp_6.1.2
BZIP2=bzip2_1.0.6
LIBXML=libxml2_2.9.4

if [ "$BUILD_OS" != "minoca" ]; then
    export ac_cv_php_xml2_config_path=$DEPENDROOT/usr/bin/xml2-config
fi

export LD_LIBRARY_PATH="$DEPENDROOT/usr/lib"
cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$NCURSES"
    extract_dependency "$READLINE"
    extract_dependency "$LIBZ"
    extract_dependency "$LIBGMP"
    extract_dependency "$BZIP2"
    extract_dependency "$LIBXML"
    export CPPFLAGS="$CPPFLAGS -I$DEPENDROOT/usr/include -I$DEPENDROOT/usr/include/libxml2"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"

    ##
    ## -lminocaos is needed in LIBS because pcre uses __clear_cache.
    ##

    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --program-prefix= \
                                     --disable-static \
                                     --enable-fpm \
                                     --with-fpm-user=www-data \
                                     --with-fpm-group=www-data \
                                     --with-zlib \
                                     --enable-bcmath \
                                     --with-bz2 \
                                     --with-gmp="$DEPENDROOT/usr" \
                                     --enable-calendar \
                                     --enable-dba=shared \
                                     --enable-ftp \
                                     --with-readline \
                                     --prefix="/usr" \
                                     --datadir="/usr/share/php" \
                                     --mandir="/usr/share/man" \
                                     --with-config-file-path="/etc" \
                                     --sysconfdir="/etc" \
                                     --localstatedir=/var \
                                     --with-libxml-dir="$DEPENDROOT/usr" \
                                     CPPFLAGS="$CPPFLAGS" \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS" \
                                     LIBS="-lminocaos"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install INSTALL_ROOT="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

