##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes font-xfree86-type1-1.0.4
##
## Author:
##
##     Evan Green 4-Apr-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh
. ../x_common.sh

export LD_LIBRARY_PATH=$DEPENDROOT/usr/lib
export PATH=$PATH:$DEPENDROOT/usr/bin

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency xproto_7.0.31
    extract_dependency util-macros_1.19.1
    extract_dependency mkfontdir_1.0.7
    extract_dependency mkfontscale_1.1.2

    extract_dependency libfreetype_2.7.1
    sh ${SOURCE_DIRECTORY}/configure $XORG_CONFIG \
                            --with-fontrootdir='${prefix}/share/fonts/X11'

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

