##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the encodings package.
##
## Author:
##
##     Evan Green 4-Apr-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

##
## Avoid trailing slashes in the cp commands, as cp on Darwin copies the
## contents of the directory (rather than the directory itself) if there are
## trailing slashes on either the source or the destination.
##

rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/usr"
rm -f $BUILD_DIRECTORY/usr/share/fonts/X11/Type1/fonts.dir
rm -f $BUILD_DIRECTORY/usr/share/fonts/X11/Type1/fonts.scale
cp -Rpv "$BUILD_DIRECTORY/usr" "$PACKAGE_DIRECTORY"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: font-xfree86-type1
Depends: mkfontdir, mkfontscale, fontconfig
Priority: optional
Version: 1.0.4
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://www.x.org/pub/individual/font/font-xfree86-type1-1.0.4.tar.bz2
Installed-Size: $INSTALLED_SIZE
Description: XFree86 Type 1 font
_EOS

cat > "$PACKAGE_DIRECTORY/CONTROL/postinst" <<"_EOS"
#!/bin/sh

mkfontscale $PKG_ROOT/usr/share/fonts/X11/Type1
mkfontdir $PKG_ROOT/usr/share/fonts/X11/Type1
fc-cache
_EOS

chmod +x "$PACKAGE_DIRECTORY/CONTROL/postinst"
cp -p "$PACKAGE_DIRECTORY/CONTROL/postinst" "$PACKAGE_DIRECTORY/CONTROL/postrm"

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

