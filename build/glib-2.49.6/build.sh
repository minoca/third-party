##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the gettext package.
##
## Author:
##
##     Evan Green 29-Aug-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBICONV=libiconv_1.14
PCRE=libpcre_8.39
LIBFFI=libffi_3.2.1
LIBZ=libz_1.2.11
GETTEXT=gettext_0.19.8.1
LIBGCC=libgcc_6.3.0

if [ "$BUILD_OS" != "minoca" ] ; then
    export glib_cv_stack_grows=no
    export glib_cv_uscore=no
    export ac_cv_path_MSGFMT=msgfmt

else
    export ac_cv_path_MSGFMT=$DEPENDROOT/usr/bin/msgfmt
    export LD_LIBRARY_PATH=$DEPENDROOT/usr/lib
fi

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$LIBICONV"
    extract_dependency "$PCRE"
    extract_dependency "$LIBFFI"
    extract_dependency "$LIBZ"
    extract_dependency "$GETTEXT"
    extract_dependency "$LIBGCC" || echo "Ignoring libgcc."
    export CPPFLAGS="$CPPFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    export ZLIB_CFLAGS=" "
    export ZLIB_LIBS="-lz"
    export LIBFFI_CFLAGS=" "
    export LIBFFI_LIBS="-lffi"
    export PCRE_CFLAGS=" "
    export PCRE_LIBS="-lpcre"
    export LT_SYS_LIBRARY_PATH="$DEPENDROOT/usr/lib"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --disable-rebuilds \
                                     --disable-static \
                                     --disable-rpath \
                                     --with-libiconv=gnu \
                                     --with-threads=posix \
                                     --with-pcre=system \
                                     --with-python=python \
                                     --prefix="/usr" \
                                     CFLAGS="$CFLAGS" \
                                     CPPFLAGS="$CPPFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install-strip DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

