##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the pkg-config package.
##
## Author:
##
##     Evan Green 15-Aug-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBICONV=libiconv_1.14

if [ "$BUILD_OS" != "minoca" ]; then
    export glib_cv_stack_grows=no
    export glib_cv_uscore=no
    export ac_cv_func_posix_getpwuid_r=yes
    export ac_cv_func_posix_getgrgid_r=yes
fi

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure-tools)
    export CFLAGS="$CFLAGS -I$OUTPUT_DIRECTORY/build/include"
    export LDFLAGS="$LDFLAGS -L$OUTPUT_DIRECTORY/build/lib"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --prefix="$OUTPUT_DIRECTORY" \
                                     --with-internal-glib \
                                     --enable-define_prefix=yes \
                                     --with-libiconv=gnu \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  configure)
    extract_dependency "$LIBICONV"
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     --with-internal-glib \
                                     --enable-define_prefix=yes \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  build-tools)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install
    if [ "$BUILD_OS" = "win32" ]; then
        cp -pv $BUILD_DIRECTORY/.libs/pkg-config.exe $OUTPUT_DIRECTORY/bin/
    fi

    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

