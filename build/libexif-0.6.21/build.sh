##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the libexif package.
##
## Author:
##
##     Chris Stevens 23-Jan-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --prefix="/usr" \
                                     --disable-nls \
                                     --disable-static

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install-strip DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

