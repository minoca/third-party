##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the EXIF library.
##
## Author:
##
##     Chris Stevens 23-Jan-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

rm -f "$BUILD_DIRECTORY"/usr/lib/*.la

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rp "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libexif
Priority: optional
Version: 0.6.21
Architecture: $PACKAGE_ARCH
Maintainer: Chris Stevens <chris@minocacorp.com>
Section: main
Source: https://sourceforge.net/projects/libexif/files/libexif/0.6.21/libexif-0.6.21.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Library to handle EXIF metadata for images.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

