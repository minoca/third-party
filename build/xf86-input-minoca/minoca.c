/*++

Copyright (c) 2017 Minoca Corp.

    This file is licensed under the terms of the GNU General Public License
    version 3. Alternative licensing terms are available. Contact
    info@minocacorp.com for details. See the LICENSE file at the root of this
    project for complete licensing information.

Module Name:

    minoca.c

Abstract:

    This module implements an X input driver for Minoca OS.

Author:

    Evan Green 5-Apr-2017

Environment:

    X

--*/

//
// ------------------------------------------------------------------- Includes
//

#include <xorg-server.h>
#include <X11/keysym.h>

#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

#include <xf86.h>
#include <xf86Xinput.h>
#include <exevents.h>
#include <xorgVersion.h>
#include <xkbsrv.h>
#include <xserver-properties.h>

//
// TODO: Figure out what to do about the fact that minoca/lib/types.h collides
// with X's types and defines.
//

#define VOID void

typedef short SHORT, *PSHORT;
typedef unsigned short USHORT, *PUSHORT;
typedef int LONG, *PLONG;
typedef unsigned int ULONG, *PULONG;
typedef long long LONGLONG, *PLONGLONG;
typedef unsigned long long ULONGLONG, *PULONGLONG;

typedef void *PVOID;
typedef PVOID HANDLE, *PHANDLE;

#include <minoca/lib/status.h>
#include <minoca/usrinput/usrinput.h>

//
// --------------------------------------------------------------------- Macros
//

//
// Right now the information gathering function is not needed.
//

#define MinocaCacheDeviceInfo(_Info) 0

//
// ---------------------------------------------------------------- Definitions
//

#define PACKAGE_VERSION_MAJOR 1
#define PACKAGE_VERSION_MINOR 0
#define PACKAGE_VERSION_PATCHLEVEL 0

#define MIN_KEYCODE 8

#define XI86_SEND_DRAG_EVENTS   0x08

#ifndef XI86_SERVER_FD
#define XI86_SERVER_FD 0x20
#endif

#define MINOCA_INPUT_KEYBOARD_EVENTS 0x00000001
#define MINOCA_INPUT_BUTTON_EVENTS 0x00000002
#define MINOCA_INPUT_RELATIVE_EVENTS 0x00000004
#define MINOCA_INPUT_INITIALIZED 0x00000008
#define MINOCA_INPUT_CALIBRATED 0x00000010

#define MINOCA_MAX_BUTTONS 32
#define MINOCA_MAX_QUEUE 32

#define MINOCA_DEFAULT_USER_INPUT_DEVICE "/dev/Pipe/UserInput"

//
// ------------------------------------------------------ Data Type Definitions
//

/*++

Structure Description:

    This structure stores the pointer calibration values.

Members:

    MinX - Stores the minimum X value.

    MaxX - Stores the maximum X Value.

    MinY - Stores the minimum Y value.

    MaxY - Stores the maximum Y Value.

--*/

typedef struct _MINOCA_INPUT_CALIBRATION {
    int MinX;
    int MaxX;
    int MinY;
    int MaxY;
} MINOCA_INPUT_CALIBRATION, *PMINOCA_INPUT_CALIBRATION;

/*++

Structure Description:

    This structure stores the mouse scroll values.

Members:

    DeltaY - Stores the vertical delta.

    DeltaX - Stores the horizontal delta.

    DeltaDial - Stores the dial delta.

--*/

typedef struct _MINOCA_INPUT_SCROLL {
    int DeltaY;
    int DeltaX;
    int DeltaDial;
} MINOCA_INPUT_SCROLL, *PMINOCA_INPUT_SCROLL;

/*++

Structure Description:

    This structure stores the context for a Minoca X input driver.

Members:

    Device - Stores the selected device name.

    TypeName - Stores the type name.

    Flags - Stores a set of flags. See MINOCA_INPUT_* definitions.

    ButtonCount - Stores the number of buttons in the mouse.

    SwapAxes - Stores a boolean indicating whether or not to swap the X and Y
        axes.

    InvertX - Stores a boolean indicating whether or not to invert the X axis.

    InvertY - Stores a boolean indicating whether or not to invert the Y axis.

    Resolution - Stores the resolution of the mouse device.

    Calibration - Stores the calibration values.

    Scroll - Stores the smooth scrolling parameters.

    ValuatorCount - Stores the count of movement axes.

    RelativeValuators - Stores the mask of relative valuators.

    LastButtons - Stores the previous state of the buttons, used to determine
        what's different.

--*/

typedef struct _MINOCA_INPUT {
    char *Device;
    char *TypeName;
    int Flags;
    int ButtonCount;
    BOOL SwapAxes;
    BOOL InvertX;
    BOOL InvertY;
    int Resolution;
    MINOCA_INPUT_CALIBRATION Calibration;
    MINOCA_INPUT_SCROLL Scroll;
    int ValuatorCount;
    ValuatorMask *RelativeValuators;
    unsigned char ButtonMap[MINOCA_MAX_BUTTONS];
    USHORT LastButtons;
} MINOCA_INPUT, *PMINOCA_INPUT;

//
// ----------------------------------------------- Internal Function Prototypes
//

pointer
MinocaPlug (
    pointer Module,
    pointer Options,
    int *Errmaj,
    int *Errmin
    );

void
MinocaUnplug (
    pointer ModuleContext
    );

int
MinocaPreInit (
    InputDriverPtr Driver,
    InputInfoPtr Info,
    int Flags
    );

void
MinocaUnInit (
    InputDriverPtr Driver,
    InputInfoPtr Info,
    int Flags
    );

int
MinocaDeviceControl (
    DeviceIntPtr Device,
    int What
    );

int
MinocaSwitchMode (
    ClientPtr Client,
    DeviceIntPtr Device,
    int Mode
    );

void
MinocaReadInput (
    InputInfoPtr Info
    );

PMINOCA_INPUT
MinocaInputAllocate (
    InputInfoPtr Info
    );

void
MinocaCloseDevice (
    InputInfoPtr Info
    );

int
MinocaOpenDevice (
    InputInfoPtr Info
    );

int
MinocaProbe (
    InputInfoPtr Info
    );

int
MinocaDeviceOn (
    DeviceIntPtr Device
    );

int
MinocaDeviceInit (
    DeviceIntPtr Device
    );

void
MinocaInitButtonMapping (
    InputInfoPtr Info
    );

int
MinocaAddKeyboard (
    DeviceIntPtr Device,
    PMINOCA_INPUT Context
    );

int
MinocaAddButtons (
    DeviceIntPtr Device,
    PMINOCA_INPUT Context
    );

int
MinocaAddRelativeMovement (
    DeviceIntPtr Device,
    PMINOCA_INPUT Context
    );

int
MinocaSetScrollValuators (
    DeviceIntPtr Device,
    PMINOCA_INPUT Context
    );

void
MinocaInitAxesLabels (
    PMINOCA_INPUT Context,
    int Mode,
    int AtomCount,
    Atom *Atoms
    );

void
MinocaInitButtonLabels (
    PMINOCA_INPUT Context,
    int AtomCount,
    Atom *Atoms
    );

void
MinocaKeyboardControl (
    DeviceIntPtr Device,
    KeybdCtrl *Control
    );

void
MinocaPtrControl (
    DeviceIntPtr Device,
    PtrCtrl *Control
    );

void
MinocaSetCalibration (
    InputInfoPtr Info,
    BOOL SetUp,
    int Calibration[4]
    );

void
MinocaProcessEvent (
    InputInfoPtr Info,
    PUSER_INPUT_EVENT Event
    );

void
MinocaProcessKeyboardEvent (
    InputInfoPtr Info,
    PUSER_INPUT_EVENT Event
    );

void
MinocaProcessMouseEvent (
    InputInfoPtr Info,
    PUSER_INPUT_EVENT Event
    );

//
// -------------------------------------------------------------------- Globals
//

static XF86ModuleVersionInfo minocaVersionRec = {
    "minoca",
    MODULEVENDORSTRING,
    MODINFOSTRING1,
    MODINFOSTRING2,
    XORG_VERSION_CURRENT,
    PACKAGE_VERSION_MAJOR, PACKAGE_VERSION_MINOR, PACKAGE_VERSION_PATCHLEVEL,
    ABI_CLASS_XINPUT,
    ABI_XINPUT_VERSION,
    MOD_CLASS_XINPUT,
    {0, 0, 0, 0}
};

_X_EXPORT XF86ModuleData minocaModuleData = {
    &minocaVersionRec,
    MinocaPlug,
    MinocaUnplug
};

_X_EXPORT InputDriverRec minocaInputDriver = {
    1,
    "minoca",
    NULL,
    MinocaPreInit,
    MinocaUnInit,
    NULL,
    NULL,
#ifdef XI86_DRV_CAP_SERVER_FD
    XI86_DRV_CAP_SERVER_FD
#endif
};

//
// ------------------------------------------------------------------ Functions
//

pointer
MinocaPlug (
    pointer Module,
    pointer Options,
    int *Errmaj,
    int *Errmin
    )

/*++

Routine Description:

    This routine is called when the input module is initialized. It performs
    global initialization.

Arguments:

    Module - Supplies a pointer identifying this module.

    Options - Supplies the options.

    Errmaj - Supplies a pointer where the major error number can be returned.

    Errmin - Supplies a pointer where teh minor error number can be returned.

Return Value:

    None.

--*/

{

    xf86AddInputDriver(&minocaInputDriver, Module, 0);
    return Module;
}

void
MinocaUnplug (
    pointer ModuleContext
    )

/*++

Routine Description:

    This routine is called when the module is removed.

Arguments:

    ModuleContext - Supplies a context pointer.

Return Value:

    None.

--*/

{

    return;
}

int
MinocaPreInit (
    InputDriverPtr Driver,
    InputInfoPtr Info,
    int Flags
    )

/*++

Routine Description:

    This routine initializes the driver.

Arguments:

    Driver - Supplies a pointer to the driver information.

    Info - Supplies a pointer to the input information.

    Flags - Supplies a bitfield of flags.

Return Value:

    Status code.

--*/

{

    PMINOCA_INPUT Context;
    int Result;

    Result = BadAlloc;
    Context = MinocaInputAllocate(Info);
    if (Context == NULL) {
        goto PreInitEnd;
    }

    Info->private = Context;
    Info->type_name = "Minoca";
    Info->device_control = MinocaDeviceControl;
    Info->read_input = MinocaReadInput;
    Info->switch_mode = MinocaSwitchMode;
    Result = MinocaOpenDevice(Info);
    if (Result != Success) {
        goto PreInitEnd;
    }

    MinocaInitButtonMapping(Info);
    if ((MinocaCacheDeviceInfo(Info) != 0) ||
        (MinocaProbe(Info) != 0)) {

        Result = BadMatch;
        goto PreInitEnd;
    }

    Context->TypeName = xf86SetStrOption(Info->options,
                                         "TypeName",
                                         Info->type_name);

    Info->type_name = Context->TypeName;

PreInitEnd:
    if (Result != Success) {
        MinocaCloseDevice(Info);
    }

    return Result;
}

void
MinocaUnInit (
    InputDriverPtr Driver,
    InputInfoPtr Info,
    int Flags
    )

/*++

Routine Description:

    This routine uninitializes the driver.

Arguments:

    Driver - Supplies a pointer to the driver information.

    Info - Supplies a pointer to the input information.

    Flags - Supplies a bitfield of flags.

Return Value:

    Status code.

--*/

{

    PMINOCA_INPUT Context;

    Context = NULL;
    if (Info != NULL) {
        Context = Info->private;
    }

    if (Context != NULL) {
        free(Context->TypeName);
        Context->TypeName = NULL;
    }

    xf86DeleteInput(Info, Flags);
    return;
}

int
MinocaDeviceControl (
    DeviceIntPtr Device,
    int What
    )

/*++

Routine Description:

    This routine responds to device control requests.

Arguments:

    Device - Supplies a pointer to the device.

    What - Supplies the control request.

Return Value:

    Status code.

--*/

{

    PMINOCA_INPUT Context;
    InputInfoPtr Info;
    int Result;

    Info = Device->public.devicePrivate;
    Context = Info->private;
    switch (What) {
    case DEVICE_INIT:
        Result = MinocaDeviceInit(Device);
        break;

    case DEVICE_ON:
        Result = MinocaDeviceOn(Device);
        break;

    case DEVICE_OFF:
        if (Info->fd >= 0) {
            xf86RemoveEnabledDevice(Info);
            MinocaCloseDevice(Info);
        }

        Context->Flags &= ~MINOCA_INPUT_INITIALIZED;
        Device->public.on = FALSE;
        Result = Success;
        break;

    case DEVICE_CLOSE:
        xf86IDrvMsg(Info, X_INFO, "Close\n");
        MinocaCloseDevice(Info);
        valuator_mask_free(&(Context->RelativeValuators));
        Result = Success;
        break;

    default:
        Result = BadValue;
        break;
    }

    return Result;
}

int
MinocaSwitchMode (
    ClientPtr Client,
    DeviceIntPtr Device,
    int Mode
    )

/*++

Routine Description:

    This routine switches pointer modes.

Arguments:

    Client - Supplies the client pointer.

    Device - Supplies the device pointer.

    Mode - Supplies the mode to switch to. Valid values are Relative and
        Absolute.

Return Value:

    Status code.

--*/

{

    PMINOCA_INPUT Context;
    InputInfoPtr Info;

    Info = Device->public.devicePrivate;
    Context = Info->private;

    if ((Context->Flags & MINOCA_INPUT_RELATIVE_EVENTS) != 0) {
        if (Mode == Relative) {
            return Success;
        }
    }

    return XI_BadMode;
}

void
MinocaReadInput (
    InputInfoPtr Info
    )

/*++

Routine Description:

    This routine implements the meaty function that reads an input event and
    shepherds it off to X.

Arguments:

    Info - Supplies the input info pointer.

Return Value:

    None.

--*/

{

    ssize_t BytesRead;
    USER_INPUT_EVENT Event;

    while (TRUE) {
        errno = 0;
        do {
            BytesRead = read(Info->fd, &Event, sizeof(Event));

        } while ((BytesRead < 0) && (errno == EINTR));

        if ((BytesRead == 0) ||
            ((BytesRead < 0) &&
             ((errno == EAGAIN) || (errno == EWOULDBLOCK)))) {

            break;
        }

        if (BytesRead != sizeof(Event)) {
            xf86IDrvMsg(Info,
                        X_ERROR,
                        "Failed to read input pipe. Read %d bytes: %s\n",
                        (int)BytesRead,
                        strerror(errno));

            break;
        }

        MinocaProcessEvent(Info, &Event);
    }

    return;
}

//
// --------------------------------------------------------- Internal Functions
//

PMINOCA_INPUT
MinocaInputAllocate (
    InputInfoPtr Info
    )

/*++

Routine Description:

    This routine allocates and initializes the context structure.

Arguments:

    Info - Supplies a pointer to the input information.

Return Value:

    Returns a pointer to the context on success.

    NULL on allocation failure.

--*/

{

    PMINOCA_INPUT Input;

    Input = calloc(sizeof(MINOCA_INPUT), 1);
    if (Input == NULL) {
        return NULL;
    }

    return Input;
}

void
MinocaCloseDevice (
    InputInfoPtr Info
    )

/*++

Routine Description:

    This routine closes the input stream.

Arguments:

    Info - Supplies a pointer to the input information.

Return Value:

    None.

--*/

{

    if (((Info->flags & XI86_SERVER_FD) == 0) && (Info->fd >= 0)) {
        close(Info->fd);
        Info->fd = -1;
    }

    return;
}

int
MinocaOpenDevice (
    InputInfoPtr Info
    )

/*++

Routine Description:

    This routine opens the input stream.

Arguments:

    Info - Supplies a pointer to the input information.

Return Value:

    Status code.

--*/

{

    PMINOCA_INPUT Context;
    char *Device;

    Context = Info->private;
    Device = Context->Device;
    if (Device == NULL) {
        Device = xf86CheckStrOption(Info->options, "Device", NULL);
        if (Device == NULL) {
            Device = MINOCA_DEFAULT_USER_INPUT_DEVICE;
        }

        Context->Device = Device;
        xf86IDrvMsg(Info, X_CONFIG, "Device: \"%s\"\n", Device);
    }

    if (((Info->flags & XI86_SERVER_FD) == 0) && (Info->fd < 0)) {
        do {
            Info->fd = open(Device, O_RDWR | O_NONBLOCK, 0);

        } while ((Info->fd < 0) && (errno == EINTR));
    }

    if (Info->fd < 0) {
        xf86IDrvMsg(Info,
                    X_ERROR,
                    "Unable to open input device \"%s\"\n",
                    Device);

        return BadValue;
    }

    return Success;
}

int
MinocaProbe (
    InputInfoPtr Info
    )

/*++

Routine Description:

    This routine probes to gather information about the input device.

Arguments:

    Info - Supplies a pointer to the input information.

Return Value:

    Status code.

--*/

{

    int ButtonCount;
    int Calibration[4];
    int CalibrationCount;
    PMINOCA_INPUT Context;
    char *String;

    Context = Info->private;
    ButtonCount = 7;
    Context->Flags |= MINOCA_INPUT_BUTTON_EVENTS;
    Context->ButtonCount = ButtonCount;
    Context->Flags |= MINOCA_INPUT_RELATIVE_EVENTS;
    Context->Flags |= MINOCA_INPUT_KEYBOARD_EVENTS;
    Context->InvertX = xf86SetBoolOption(Info->options, "InvertX", FALSE);
    Context->InvertY = xf86SetBoolOption(Info->options, "InvertY", FALSE);
    Context->SwapAxes = xf86SetBoolOption(Info->options, "SwapAxes", FALSE);
    Context->Resolution = xf86SetIntOption(Info->options, "Resolution", 0);
    if (Context->Resolution < 0) {
        xf86IDrvMsg(Info, X_ERROR, "Resolution must be positive");
        Context->Resolution = 0;
    }

    String = xf86CheckStrOption(Info->options, "Calibration", NULL);
    if (String != NULL) {
        CalibrationCount = sscanf(String,
                                  "%d %d %d %d",
                                  &(Calibration[0]),
                                  &(Calibration[1]),
                                  &(Calibration[2]),
                                  &(Calibration[3]));

        if (CalibrationCount == 4) {
            MinocaSetCalibration(Info, TRUE, Calibration);

        } else {
            xf86IDrvMsg(Info,
                        X_ERROR,
                        "Invalid calibration value. Should be four integers.");
        }
    }

    Info->flags |= XI86_SEND_DRAG_EVENTS;
    Info->type_name = XI_KEYBOARD;
    Context->Scroll.DeltaY = xf86SetIntOption(Info->options,
                                              "VertScrollDelta",
                                              1);

    Context->Scroll.DeltaX = xf86SetIntOption(Info->options,
                                              "HorizScrollDelta",
                                              1);

    Context->Scroll.DeltaDial = xf86SetIntOption(Info->options,
                                                 "DialDelta",
                                                 1);

    return Success;
}

int
MinocaDeviceOn (
    DeviceIntPtr Device
    )

/*++

Routine Description:

    This routine turns the device "on".

Arguments:

    Device - Supplies a pointer to the device.

Return Value:

    Status code.

--*/

{

    PMINOCA_INPUT Context;
    InputInfoPtr Info;
    int Result;

    Info = Device->public.devicePrivate;
    Context = Info->private;
    Result = MinocaOpenDevice(Info);
    if (Result != Success) {
        return Result;
    }

    //xf86FlushInput(Info->fd);
    xf86AddEnabledDevice(Info);
    Context->Flags |= MINOCA_INPUT_INITIALIZED;
    Device->public.on = TRUE;
    return Success;
}

int
MinocaDeviceInit (
    DeviceIntPtr Device
    )

/*++

Routine Description:

    This routine initializes a device.

Arguments:

    Device - Supplies a pointer to the device.

Return Value:

    Status code.

--*/

{

    PMINOCA_INPUT Context;
    InputInfoPtr Info;

    Info = Device->public.devicePrivate;
    Context = Info->private;
    if ((Context->Flags & MINOCA_INPUT_KEYBOARD_EVENTS) != 0) {
        MinocaAddKeyboard(Device, Context);
    }

    if ((Context->Flags & MINOCA_INPUT_BUTTON_EVENTS) != 0) {
        MinocaAddButtons(Device, Context);
    }

    if ((Context->Flags & MINOCA_INPUT_RELATIVE_EVENTS) != 0) {
        MinocaAddRelativeMovement(Device, Context);
    }

    return Success;
}

void
MinocaInitButtonMapping (
    InputInfoPtr Info
    )

/*++

Routine Description:

    This routine sets up a custom button mapping if needed.

Arguments:

    Info - Supplies a pointer to the input information.

Return Value:

    None.

--*/

{

    int Button;
    int ButtonCount;
    PMINOCA_INPUT Context;
    int Index;
    char *Map;
    char *Mapping;
    char *Search;

    ButtonCount = 0;
    Context = Info->private;
    Mapping = xf86CheckStrOption(Info->options, "ButtonMapping", NULL);
    if (Mapping != NULL) {
        xf86IDrvMsg(Info, X_CONFIG, "ButtonMapping \"%s\"\n", Mapping);
        Map = Mapping;
        do {
            Button = strtol(Map, &Search, 10);
            if ((Search == Map) || (Button < 0) ||
                (Button >= MINOCA_MAX_BUTTONS)) {

                xf86IDrvMsg(Info, X_ERROR, "Invalid button mapping");
                ButtonCount = 1;
                break;
            }

            Context->ButtonMap[ButtonCount] = Button;
            ButtonCount += 1;
            Map = Search;

        } while ((Search != NULL) && (*Search != '\0') &&
                 (ButtonCount < MINOCA_MAX_BUTTONS));

        free(Mapping);
    }

    //
    // Initialize the rest to the identity map.
    //

    for (Index = ButtonCount; Index < MINOCA_MAX_BUTTONS; Index += 1) {
        Context->ButtonMap[Index] = Index;
    }

    return;
}

int
MinocaAddKeyboard (
    DeviceIntPtr Device,
    PMINOCA_INPUT Context
    )

/*++

Routine Description:

    This routine registers keyboard capabilities with the system.

Arguments:

    Device - Supplies a pointer to the device.

    Context - Supplies the device-specific context.

Return Value:

    Status code.

--*/

{

    XkbRMLVOSet Defaults;
    InputInfoPtr Info;
    XkbRMLVOSet Set;
    int Result;

    Result = Success;
    Info = Device->public.devicePrivate;
    XkbGetRulesDflts(&Defaults);
    memset(&Set, 0, sizeof(Set));
    xf86ReplaceStrOption(Info->options, "xkb_rules", "minoca");
    Set.rules = xf86SetStrOption(Info->options, "xkb_rules", NULL);
    Set.model = xf86SetStrOption(Info->options, "xkb_model", Defaults.model);
    Set.layout = xf86SetStrOption(Info->options, "xkb_layout", Defaults.layout);
    Set.variant = xf86SetStrOption(Info->options,
                                   "xkb_variant",
                                   Defaults.variant);

    Set.options = xf86SetStrOption(Info->options,
                                   "xkb_options",
                                   Defaults.options);

    Result = InitKeyboardDeviceStruct(Device,
                                      &Set,
                                      NULL,
                                      MinocaKeyboardControl);

    if (Result == 0) {
        Result = !Success;

    } else {
        Result = Success;
    }

    XkbFreeRMLVOSet(&Set, FALSE);
    XkbFreeRMLVOSet(&Defaults, FALSE);
    return Result;
}

int
MinocaAddButtons (
    DeviceIntPtr Device,
    PMINOCA_INPUT Context
    )

/*++

Routine Description:

    This routine registers relative movement capabilities with the system.

Arguments:

    Device - Supplies a pointer to the device.

    Context - Supplies the device-specific context.

Return Value:

    Status code.

--*/

{

    Atom *Labels;
    int Result;

    Labels = malloc(Context->ButtonCount * sizeof(Atom));
    if (Labels == NULL) {
        return !Success;
    }

    MinocaInitButtonLabels(Context, Context->ButtonCount, Labels);
    Result = InitButtonClassDeviceStruct(Device,
                                         Context->ButtonCount,
                                         Labels,
                                         Context->ButtonMap);

    if (Result == 0) {
        return !Success;
    }

    return Success;
}

int
MinocaAddRelativeMovement (
    DeviceIntPtr Device,
    PMINOCA_INPUT Context
    )

/*++

Routine Description:

    This routine registers relative movement capabilities with the system.

Arguments:

    Device - Supplies a pointer to the device.

    Context - Supplies the device-specific context.

Return Value:

    Status code.

--*/

{

    Atom *Atoms;
    int Axis;
    int AxisCount;
    InputInfoPtr Info;
    int Result;

    Atoms = NULL;
    Info = Device->public.devicePrivate;

    //
    // Stick to 4 axes: X, Y, Y scroll, and X scroll.
    //

    AxisCount = 4;
    Context->ValuatorCount = AxisCount;
    Context->RelativeValuators = valuator_mask_new(AxisCount);
    if (Context->RelativeValuators == NULL) {
        Result = !Success;
        goto AddRelativeMovementEnd;
    }

    Atoms = malloc(Context->ValuatorCount * sizeof(Atom));
    if (Atoms == NULL) {
        Result = !Success;
        goto AddRelativeMovementEnd;
    }

    MinocaInitAxesLabels(Context, Relative, Context->ValuatorCount, Atoms);
    Result = InitValuatorClassDeviceStruct(Device,
                                           AxisCount,
                                           Atoms,
                                           GetMotionHistorySize(),
                                           Relative);

    if (Result == 0) {
        xf86IDrvMsg(Info,
                    X_ERROR,
                    "Failed to initialize valuator class device.\n");

        Result = !Success;
        goto AddRelativeMovementEnd;
    }

    Result = InitPtrFeedbackClassDeviceStruct(Device, MinocaPtrControl);
    if (Result == 0) {
        xf86IDrvMsg(Info,
                    X_ERROR,
                    "Failed to initialize pointer feedback device.\n");

        Result = !Success;
        goto AddRelativeMovementEnd;
    }

    for (Axis = 0; Axis < AxisCount; Axis += 1) {
        xf86InitValuatorAxisStruct(Device,
                                   Axis,
                                   Atoms[Axis],
                                   -1,
                                   -1,
                                   1,
                                   0,
                                   1,
                                   Relative);

        xf86InitValuatorDefaults(Device, Axis);
    }

    MinocaSetScrollValuators(Device, Context);
    Result = Success;

AddRelativeMovementEnd:
    free(Atoms);
    if (Result != Success) {
        valuator_mask_free(&(Context->RelativeValuators));
        Context->RelativeValuators = NULL;
    }

    return Result;
}

int
MinocaSetScrollValuators (
    DeviceIntPtr Device,
    PMINOCA_INPUT Context
    )

/*++

Routine Description:

    This routine registers scroll valuators with the system.

Arguments:

    Device - Supplies a pointer to the device.

    Context - Supplies the device-specific context.

Return Value:

    Status code.

--*/

{

    //
    // The third axis is the scroll wheel.
    //

    SetScrollValuator(Device,
                      2,
                      SCROLL_TYPE_VERTICAL,
                      -Context->Scroll.DeltaY,
                      SCROLL_FLAG_PREFERRED);

    //
    // The fourth axis is the horizontal scroll wheel.
    //

    SetScrollValuator(Device,
                      3,
                      SCROLL_TYPE_HORIZONTAL,
                      Context->Scroll.DeltaX,
                      SCROLL_FLAG_NONE);


    return Success;
}

void
MinocaInitAxesLabels (
    PMINOCA_INPUT Context,
    int Mode,
    int AtomCount,
    Atom *Atoms
    )

/*++

Routine Description:

    This routine initializes the axis labels for a set of atoms.

Arguments:

    Context - Supplies the device-specific context.

    Mode - Supplies the mode: Relative or Absolute.

    AtomCount - Supplies the number of atoms in the array.

    Atoms - Supplies the array of atoms.

Return Value:

    None.

--*/

{

    memset(Atoms, 0, AtomCount * sizeof(Atom));

    //
    // Just hardcode the mappings for now.
    //

    Atoms[0] = XIGetKnownProperty(AXIS_LABEL_PROP_REL_X);
    Atoms[1] = XIGetKnownProperty(AXIS_LABEL_PROP_REL_Y);
    Atoms[2] = XIGetKnownProperty(AXIS_LABEL_PROP_REL_WHEEL);
    Atoms[3] = XIGetKnownProperty(AXIS_LABEL_PROP_REL_HWHEEL);
    return;
}

void
MinocaInitButtonLabels (
    PMINOCA_INPUT Context,
    int AtomCount,
    Atom *Atoms
    )

/*++

Routine Description:

    This routine initializes button labels.

Arguments:

    Context - Supplies the device-specific context.

    AtomCount - Supplies the number of atoms in the array.

    Atoms - Supplies the array of atoms.

Return Value:

    None.

--*/

{

    Atom AtomValue;
    int Button;

    AtomValue = XIGetKnownProperty(BTN_LABEL_PROP_BTN_UNKNOWN);
    for (Button = 0; Button < AtomCount; Button += 1) {
        Atoms[Button] = AtomValue;
    }

    if (AtomCount >= 1) {
        Atoms[0] = XIGetKnownProperty(BTN_LABEL_PROP_BTN_LEFT);
    }

    if (AtomCount >= 2) {
        Atoms[1] = XIGetKnownProperty(BTN_LABEL_PROP_BTN_RIGHT);
    }

    if (AtomCount >= 3) {
        Atoms[2] = XIGetKnownProperty(BTN_LABEL_PROP_BTN_MIDDLE);
    }

    return;
}

void
MinocaKeyboardControl (
    DeviceIntPtr Device,
    KeybdCtrl *Control
    )

/*++

Routine Description:

    This routine implements the keyboard control handler, which does things
    like sets the LEDs.

Arguments:

    Device - Supplies a pointer to the device.

    Control - Supplies the keyboard control command.

Return Value:

    None.

--*/

{

    InputInfoPtr Info;

    Info = Device->public.devicePrivate;

    //
    // TODO: Set the LED bits in Control->leds. The bits are caps, num, scroll,
    // mode, and compose, with caps == 1.
    //

    xf86IDrvMsg(Info, X_ERROR, "Keyboard LEDs not yet supported.\n");
    return;
}

void
MinocaPtrControl (
    DeviceIntPtr Device,
    PtrCtrl *Control
    )

/*++

Routine Description:

    This routine implements the pointer control handler.

Arguments:

    Device - Supplies a pointer to the device.

    Control - Supplies the pointer control command.

Return Value:

    None.

--*/

{

    return;
}

void
MinocaSetCalibration (
    InputInfoPtr Info,
    BOOL SetUp,
    int Calibration[4]
    )

/*++

Routine Description:

    This routine sets up pointer calibration.

Arguments:

    Context - Supplies the device-specific context.

    SetUp - Supplies a boolean indicating whether to set up or tear down
        calibration values.

    Calibration - Supplies the calibration values.

Return Value:

    None.

--*/

{

    PMINOCA_INPUT Context;

    Context = Info->private;
    if (SetUp != FALSE) {
        Context->Calibration.MinX = Calibration[0];
        Context->Calibration.MaxX = Calibration[1];
        Context->Calibration.MinY = Calibration[2];
        Context->Calibration.MaxY = Calibration[3];
        Context->Flags |= MINOCA_INPUT_CALIBRATED;

    } else {
        Context->Flags &= ~MINOCA_INPUT_CALIBRATED;
        Context->Calibration.MinX = 0;
        Context->Calibration.MaxX = 0;
        Context->Calibration.MinY = 0;
        Context->Calibration.MaxY = 0;
    }

    return;
}

void
MinocaProcessEvent (
    InputInfoPtr Info,
    PUSER_INPUT_EVENT Event
    )

/*++

Routine Description:

    This routine processes an input event.

Arguments:

    Info - Supplies the input info.

    Event - Supplies a pointer to the event.

Return Value:

    None.

--*/

{

    switch (Event->DeviceType) {
    case UserInputDeviceKeyboard:
        MinocaProcessKeyboardEvent(Info, Event);
        break;

    case UserInputDeviceMouse:
        MinocaProcessMouseEvent(Info, Event);
        break;

    default:
        break;
    }

    return;
}

void
MinocaProcessKeyboardEvent (
    InputInfoPtr Info,
    PUSER_INPUT_EVENT Event
    )

/*++

Routine Description:

    This routine processes a keyboard input event.

Arguments:

    Info - Supplies the input info.

    Event - Supplies a pointer to the event.

Return Value:

    None.

--*/

{

    int Code;
    int Value;

    Value = 0;
    if (Event->EventType == UserInputEventKeyDown) {
        Value = 1;
    }

    Code = Event->U.Key;
    xf86PostKeyboardEvent(Info->dev, Code + MIN_KEYCODE, Value);
    return;
}

void
MinocaProcessMouseEvent (
    InputInfoPtr Info,
    PUSER_INPUT_EVENT Event
    )

/*++

Routine Description:

    This routine processes a mouse input event.

Arguments:

    Info - Supplies the input info.

    Event - Supplies a pointer to the event.

Return Value:

    None.

--*/

{

    int Bit;
    USHORT ButtonDelta;
    int ButtonValue;
    PMINOCA_INPUT Context;
    double Delta;
    BOOL Motion;
    PMOUSE_EVENT Mouse;
    LONG Swap;

    Motion = FALSE;
    Context = Info->private;
    Mouse = &(Event->U.Mouse);
    if (Context->SwapAxes != FALSE) {
        Swap = Mouse->MovementX;
        Mouse->MovementX = Mouse->MovementY;
        Mouse->MovementY = Swap;
    }

    //
    // Process movement.
    //

    if (Mouse->MovementX != 0) {
        Motion = TRUE;
        if (Context->InvertX != FALSE) {
            Mouse->MovementX *= -1;
        }

        Delta = Mouse->MovementX;
        if (valuator_mask_isset(Context->RelativeValuators, 0)) {
            Delta += valuator_mask_get(Context->RelativeValuators, 0);
        }

        valuator_mask_set(Context->RelativeValuators, 0, Delta);
    }

    if (Mouse->MovementY != 0) {
        Motion = TRUE;
        if (Context->InvertY != FALSE) {
            Mouse->MovementY *= -1;
        }

        Delta = Mouse->MovementY;
        if (valuator_mask_isset(Context->RelativeValuators, 1)) {
            Delta += valuator_mask_get(Context->RelativeValuators, 1);
        }

        valuator_mask_set(Context->RelativeValuators, 1, Delta);
    }

    //
    // Process scroll wheel action.
    //

    if (Mouse->ScrollY != 0) {
        Motion = TRUE;
        Delta = Mouse->ScrollY;
        if (valuator_mask_isset(Context->RelativeValuators, 2)) {
            Delta += valuator_mask_get(Context->RelativeValuators, 2);
        }

        valuator_mask_set(Context->RelativeValuators, 2, Delta);
    }

    if (Mouse->ScrollX != 0) {
        Motion = TRUE;
        Delta = Mouse->ScrollX;
        if (valuator_mask_isset(Context->RelativeValuators, 3)) {
            Delta += valuator_mask_get(Context->RelativeValuators, 3);
        }

        valuator_mask_set(Context->RelativeValuators, 3, Delta);
    }

    if (Motion != FALSE) {
        xf86PostMotionEventM(Info->dev, Relative, Context->RelativeValuators);
        valuator_mask_zero(Context->RelativeValuators);
    }

    //
    // Process button changes.
    //

    ButtonDelta = Mouse->Buttons ^ Context->LastButtons;
    Context->LastButtons = Mouse->Buttons;
    Bit = 0;
    while (ButtonDelta != 0) {
        if ((ButtonDelta & (1 << Bit)) != 0) {
            ButtonValue = (Mouse->Buttons & (1 << Bit)) != 0;
            xf86PostButtonEvent(Info->dev,
                                Relative,
                                Bit + 1,
                                ButtonValue,
                                0,
                                0);

            ButtonDelta &= ~(1 << Bit);
        }

        Bit += 1;
    }

    return;
}

