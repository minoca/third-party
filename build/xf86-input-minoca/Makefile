################################################################################
#
#   Copyright (c) 2017 Minoca Corp. All Rights Reserved.
#
#   Module Name:
#
#       xf86-input-minoca
#
#   Abstract:
#
#       This makefile is responsible for building the Minoca X input driver.
#
#   Author:
#
#       Evan Green 6-Apr-2017
#
#   Environment:
#
#       Build
#
################################################################################

include ../common.mk

PACKAGE := xf86-input-minoca

BUILDROOT := $(OBJROOT)/$(PACKAGE).build
BINROOT := $(BUILDROOT)/build.out
PKGROOT := $(BUILDROOT)/build.pkg

CC := $(TARGET)-gcc

VPATH = $(BUILDROOT)

.PHONY: all clean package

OBJS := minoca.o     \

SHAREDLIB = $(BUILDROOT)/minoca_drv.so

INCLUDES = -I$(DEPENDROOT)/usr/include -I$(DEPENDROOT)/usr/include/xorg \
 -I$(DEPENDROOT)/usr/include/pixman-1

LIBS =

EXTRA_CFLAGS := $(INCLUDES) -fPIC -O2 -g -Wall

all: | $(BUILDROOT)
	$(MAKE) build
	$(MAKE) package

build: $(SHAREDLIB) | $(BUILDROOT) $(BINROOT)
	@echo Building $(PACKAGE)
	cp -v $(SHAREDLIB) $(BINROOT)/
	@echo Done building $(PACKAGE)

$(OBJS): | $(BUILDROOT)

clean:
	rm -rf $(BUILDROOT)

package:
	sh ./package.sh "$(BINROOT)" "$(PKGROOT)"

$(SHAREDLIB): $(OBJS)
	cd $(BUILDROOT) && $(CC) -shared -o $@ -Wl,-soname -Wl,$@ $^ $(LIBS)

%.o:%.c
	@echo Compiling - $<
	@$(CC) $(CPPFLAGS) $(CFLAGS) $(INCLUDES) $(EXTRA_CFLAGS) -c -o $(BUILDROOT)/$@ $<

$(BUILDROOT) $(BINROOT):
	mkdir -p $@
