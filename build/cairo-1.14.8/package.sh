##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the cairo library.
##
## Author:
##
##     Evan Green 19-Apr-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

##
## Avoid trailing slashes in the cp commands, as cp on Darwin copies the
## contents of the directory (rather than the directory itself) if there are
## trailing slashes on either the source or the destination.
##

rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/usr"
rm -f $BUILD_DIRECTORY/usr/lib/*.la
rm -f $BUILD_DIRECTORY/usr/lib/cairo/*.la
rm -rf $BUILD_DIRECTORY/usr/share/gtk-doc
cp -Rpv "$BUILD_DIRECTORY/usr/" "$PACKAGE_DIRECTORY/"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libcairo
Depends: libglib, libpixman, libpng, fontconfig, libz, libfreetype, libxrender, libx11, gettext, libgcc
Priority: optional
Version: 1.14.8
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://cairographics.org/releases/cairo-1.14.8.tar.xz
Installed-Size: $INSTALLED_SIZE
Description: 2D graphics library
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

