##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages automake.
##
## Author:
##
##     Evan Green 12-Jul-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

##
## Avoid trailing slashes in the cp commands, as cp on Darwin copies the
## contents of the directory (rather than the directory itself) if there are
## trailing slashes on either the source or the destination.
##

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/share" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: automake
Depends: autoconf, libgcc
Priority: optional
Version: 1.12.6
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://ftp.gnu.org/gnu/automake/automake-1.12.6.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: GNU automake tool
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

