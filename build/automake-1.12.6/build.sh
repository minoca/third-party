##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the automake packages.
##
## Author:
##
##     Evan Green 12-Jul-2017
##
## Environment:
##
##     Build with POSIX tools.
##

. ../build_common.sh

##
## Automake needs autoconf.
##

if test "x$BUILD_OS" = "xwin32"; then

    ##
    ## Unfortunately Perl backtick commands send each word as a separate
    ## argument, so simply setting it to "sh -c" would get expanded to
    ## "sh -c 'word0' '...'", when it really should be "sh -c 'word0 ...'".
    ## This "script" repackages the command line to be in a single argument.
    ##

    export PERL5SHELL='sh -c "sh -c \\"\$*\\"" dummy'
    export M4="$TOOLBINROOT/bin/m4"
    export TMPDIR="$BUILD_DIRECTORY"
    export ac_cv_path_PERL='perl'
fi

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure-tools)
    touch ${SOURCE_DIRECTORY}/Makefile.in
    export AUTOCONF="$TOOLBINROOT/bin/autoconf"
    sh ${SOURCE_DIRECTORY}/configure --prefix="$OUTPUT_DIRECTORY"
    ;;

  configure)
    extract_dependency "autoconf_2.69"
    touch ${SOURCE_DIRECTORY}/Makefile.in
    export AUTOCONF="$DEPENDROOT/usr/bin/autoconf"
    export AUTOM4TE="$DEPENDROOT/usr/bin/autom4te"
    export PERL5LIB="$DEPENDROOT/usr/share/autoconf"
    sed "s|'/usr/share/autoconf'|'$DEPENDROOT/usr/share/autoconf'|" \
      "$DEPENDROOT/usr/share/autoconf/autom4te.cfg" \
      >"$DEPENDROOT/usr/share/autoconf/autom4te.cfg2"

    export AUTOM4TE_CFG="$DEPENDROOT/usr/share/autoconf/autom4te.cfg2"
    sh ${SOURCE_DIRECTORY}/configure --prefix="/usr"
    ;;

  build-tools)
    $MAKE
    $MAKE install
    ;;

  build)
    $MAKE
    $MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

