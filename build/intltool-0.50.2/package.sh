##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the intltool library.
##
## Author:
##
##     Evan Green 2-Apr-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
rm -rf $BUILD_DIRECTORY/usr/share/man
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/share" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: intltool
Depends: perl
Priority: optional
Version: 0.50.2
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://launchpad.net/intltool/trunk/0.50.2/+download/intltool-0.50.2.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Internationalization tool.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

