##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the ninja package.
##
## Author:
##
##     Evan Green 20-Jul-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

##
## Build ninja manually to avoid a dependency on Python.
##

SOURCES="
build
build_log
clean
clparser
debug_flags
depfile_parser
deps_log
disk_interface
edit_distance
eval_env
graph
graphviz
lexer
line_printer
manifest_parser
metrics
state
util
version"

WIN32_SOURCES="
subprocess-win32
includes_normalize-win32
msvc_helper-win32
msvc_helper_main-win32"

POSIX_SOURCES="browse
subprocess-posix"

export CXXFLAGS="$CXXFLAGS -Wall -Wextra -Wno-deprecated \
-Wno-missing-field-initializers -Wno-unused-parameter -fno-rtti \
-fno-exceptions -DNINJA_PYTHON=\"python\" -DNDEBUG -I."

LDFLAGS="$LDFLAGS -L."

compile_ninja() {
    mkdir -p build

    ##
    ## Generate browse_py.h
    ##

    ${SHELL} $SOURCE_DIRECTORY/src/inline.sh kBrowsePy \
        < $SOURCE_DIRECTORY/src/browse.py > build/browse_py.h

    ##
    ## Compile sources.
    ##

    OBJS=
    for f in $SOURCES; do
      echo Compiling - $f.cc
      $CXX $CXXFLAGS -c -o $f.o $SOURCE_DIRECTORY/src/$f.cc
      OBJS="$OBJS $f.o"
    done

    ##
    ## Create the library
    ##

    echo Creating Library - libninja.a
    $AR rcs libninja.a $OBJS

    ##
    ## Link the binary.
    ##

    echo Compiling - ninja.cc
    $CXX $CXXFLAGS -c -o ninja.o $SOURCE_DIRECTORY/src/ninja.cc
    echo Linking - $OUTPUT_DIRECTORY/bin/ninja
    mkdir -p "$OUTPUT_DIRECTORY/bin"
    $CXX $CXXFLAGS $LDFLAGS -o "$OUTPUT_DIRECTORY/bin/ninja" ninja.o -lninja
}

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  build)
    SOURCES="$SOURCES $POSIX_SOURCES"
    CXX="$TARGET-g++"
    AR="$TARGET-ar"
    compile_ninja
    ;;

  build-tools)
    CXX=g++
    AR=ar
    if [ "$BUILD_OS" = "win32" ]; then
      CXXFLAGS="$CXXFLAGS -D_WIN32_WINNT=0x0501"
      SOURCES="$SOURCES $WIN32_SOURCES"

    else
      CXXFLAGS="$CXXFLAGS -DNINJA_HAVE_BROWSE"
      SOURCES="$SOURCES $POSIX_SOURCES"
    fi

    compile_ninja
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

