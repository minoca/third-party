##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the mesa demos.
##
## Author:
##
##     Evan Green 26-Apr-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh
. ../x_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency glproto_1.4.17
    extract_dependency libx11_1.6.5
    extract_dependency libxext_1.3.3
    extract_dependency dri2proto_2.8
    extract_dependency libxdamage_1.1.4
    extract_dependency libxfixes_5.0.3
    extract_dependency fixesproto_5.0
    extract_dependency damageproto_1.2.1
    extract_dependency expat_2.1.0
    extract_dependency libxdmcp_1.1.2
    extract_dependency mesa_17.0.0
    extract_dependency libglu_9.0.0
    extract_dependency libxi_1.7.9
    extract_dependency inputproto_2.3.2
    extract_dependency libglu_9.0.0
    extract_dependency freeglut_3.0.0
    extract_dependency libglew_2.0.0
    extract_dependency libfreetype_2.7.1

    extract_dependency libglib_2.49.6
    extract_dependency libharfbuzz_1.4.4
    extract_dependency xproto_7.0.31
    extract_dependency libxcb_1.12
    extract_dependency libxau_1.0.8
    extract_dependency kbproto_1.0.7
    extract_dependency xextproto_7.3.0
    extract_dependency renderproto_0.11.1
    extract_dependency libpcre_8.39
    extract_dependency gettext_0.19.8.1
    extract_dependency libiconv_1.14


    export CFLAGS="$CFLAGS"
    export CPPFLAGS="-I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=/usr/lib/gcc/$TARGET/6.3.0 \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    sh ${SOURCE_DIRECTORY}/configure $XORG_CONFIG \
                                     --disable-egl

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

