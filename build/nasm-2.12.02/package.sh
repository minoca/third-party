##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages nasm.
##
## Author:
##
##     Vasco Costa 19-Dec-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: nasm
Depends: libgcc
Priority: optional
Version: 2.12.02
Architecture: $PACKAGE_ARCH
Maintainer: Vasco Costa <gluon81@gmx.com>
Section: main
Source: http://www.nasm.us/pub/nasm/releasebuilds/2.12.02/nasm-2.12.02.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: General-purpose x86 assembler.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

