##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the gettext utility.
##
## Author:
##
##     Evan Green 29-Aug-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

rm -f "$BUILD_DIRECTORY/usr/lib/"*.la
rm -f "$BUILD_DIRECTORY/usr/lib/charset.alias"
rm -rf "$BUILD_DIRECTORY/usr/share/doc"
rm -rf "$BUILD_DIRECTORY/usr/share/man"
rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/share" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: gettext
Depends: libiconv, libxml2, libunistring, libncurses, libgcc
Priority: optional
Version: 0.19.8.1
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://ftp.gnu.org/pub/gnu/gettext/gettext-0.19.8.1.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: GNU gettext utility.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

