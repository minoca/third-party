##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the guile package.
##
## Author:
##
##     Evan Green 27-Jul-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBGMP=libgmp_6.1.2
LIBICONV=libiconv_1.14
LIBTOOL=libtool_2.4.6
LIBUNISTRING=libunistring_0.9.6
LIBFFI=libffi_3.2.1
GC=libgc_7.4.4
READLINE=libreadline_6.3

export LD_LIBRARY_PATH="$DEPENDROOT/usr/lib:$BUILD_DIRECTORY/libguile/.libs"

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency "$LIBGMP"
    extract_dependency "$LIBICONV"
    extract_dependency "$LIBTOOL"
    extract_dependency "$LIBUNISTRING"
    extract_dependency "$LIBFFI"
    extract_dependency "$READLINE"
    extract_dependency "$GC"
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export LIBFFI_LIBS=-lffi
    export LIBFFI_CFLAGS=" "
    export BDW_GC_LIBS=-lgc
    export BDW_GC_CFLAGS=" "
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     --disable-static \
                                     --disable-rpath \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install-strip DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

