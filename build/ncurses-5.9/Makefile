################################################################################
#
#   Copyright (c) 2014 Minoca Corp. All Rights Reserved.
#
#   Module Name:
#
#       ncurses-5.9
#
#   Abstract:
#
#       This makefile is responsible for building the ncurses package.
#
#   Author:
#
#       Evan Green 10-Jul-2014
#
#   Environment:
#
#       Build
#
################################################################################

include ../common.mk

PACKAGE := ncurses-5.9
PATCHED_SOURCE_DIRECTORY := $(OBJROOT)/$(PACKAGE).src
ORIGINAL_SOURCE_DIRECTORY := $(OBJROOT)/$(PACKAGE).orig
SOURCE_TARBALL := $(ROOT)/third-party/src/$(PACKAGE).tar.gz

TOOLBUILDROOT := $(OBJROOT)/$(PACKAGE).tool
TOOLBINROOT := $(OUTROOT)/tools/build
BUILDROOT := $(OBJROOT)/$(PACKAGE).build
BINROOT := $(BUILDROOT)/build.out
PKGROOT := $(BUILDROOT)/build.pkg

DIFF_FILE := $(CURDIR)/$(PACKAGE).diff

.PHONY: all clean recreate-diff tools package

all: $(BUILDROOT)/Makefile
	@echo Building $(PACKAGE)
	+sh ./build.sh "$(PATCHED_SOURCE_DIRECTORY)" "$(BUILDROOT)" "$(BINROOT)" build
	$(MAKE) package

clean:
	rm -rf $(BUILDROOT)
	rm -rf $(TOOLBUILDROOT)
	rm -rf $(PATCHED_SOURCE_DIRECTORY)
	rm -rf $(ORIGINAL_SOURCE_DIRECTORY)

package:
	sh ./package.sh "$(BINROOT)" "$(PKGROOT)"

##
## This target recreates the diff file from the patched source directory.
##

recreate-diff: $(ORIGINAL_SOURCE_DIRECTORY)
	cd $(PATCHED_SOURCE_DIRECTORY) && diff -NrU3 -x .svn ../$(PACKAGE).orig . > $(DIFF_FILE) || test $$? = "1"

##
## Unpack the source to PACKAGE.orig.
##

$(ORIGINAL_SOURCE_DIRECTORY): $(SOURCE_TARBALL)
	rm -rf $(ORIGINAL_SOURCE_DIRECTORY)
	tar -xzf $^ -C $(OBJROOT)
	mv $(OBJROOT)/$(PACKAGE) $@

tools: $(TOOLBUILDROOT)/Makefile
	@echo Building $(PACKAGE)
	+sh ./build.sh "$(PATCHED_SOURCE_DIRECTORY)" "$(TOOLBUILDROOT)" "$(TOOLBINROOT)" build-tools

$(TOOLBUILDROOT)/Makefile: | $(TOOLBINROOT) $(TOOLBUILDROOT) $(PATCHED_SOURCE_DIRECTORY)
	@echo Configuring $(PACKAGE)
	+sh ./build.sh "$(PATCHED_SOURCE_DIRECTORY)" "$(TOOLBUILDROOT)" "$(TOOLBINROOT)" configure-tools

$(BUILDROOT)/Makefile: | $(BINROOT) $(BUILDROOT) $(PATCHED_SOURCE_DIRECTORY)
	@echo Configuring $(PACKAGE)
	+sh ./build.sh "$(PATCHED_SOURCE_DIRECTORY)" "$(BUILDROOT)" "$(BINROOT)" configure

##
## Unpack the source to PACKAGE.src, then apply the patch.
##

$(PATCHED_SOURCE_DIRECTORY): $(SOURCE_TARBALL) | $(OBJROOT)
	rm -rf $(PATCHED_SOURCE_DIRECTORY)
	tar -xzf $(SOURCE_TARBALL) -C $(OBJROOT)
	mv $(OBJROOT)/$(PACKAGE) $@
	cd $(PATCHED_SOURCE_DIRECTORY) && $(PATCH) -p0 -i $(DIFF_FILE)

$(TOOLBUILDROOT) $(BUILDROOT) $(BINROOT):
	mkdir -p $@

