##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the OpenSSH package.
##
## Author:
##
##     Evan Green 13-Jan-2015
##
## Environment:
##
##     Build
##

. ../build_common.sh

OPENSSL=libopenssl_1.0.2h
ZLIB=libz_1.2.11

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$OPENSSL"
    extract_dependency "$ZLIB"

    ##
    ## TODO: Remove the -g once the hang is resolved.
    ##

    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include -g"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    export ac_cv_func_getrlimit=no
    export ac_cv_func_setrlimit=no
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     --libexecdir='${prefix}/lib/openssh' \
                                     --sysconfdir="/etc/ssh" \
                                     --with-privsep-path="/var/run/sshd" \
                                     --with-maildir="/var/mail" \
                                     --without-openssl-header-check \
                                     --enable-strip=no \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install-nokeys DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

