##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the tar utility.
##
## Author:
##
##     Evan Green 13-Feb-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY"
cp -Rpv "$BUILD_DIRECTORY/etc" "$PACKAGE_DIRECTORY"
cp -Rpv "$BUILD_DIRECTORY/usr" "$PACKAGE_DIRECTORY"

##
## Create several init scripts. They're mostly the same except for the commands
## they run.
##

mkdir -p "$PACKAGE_DIRECTORY/etc/init.d"
for step in cloud-init-local cloud-init cloud-init-config cloud-init-final; do
    case $step in
    cloud-init-local)
        CMD="init --local"
        DESC="Run pre-networking cloud-init initialization."
        ;;

    cloud-init)
        CMD="init"
        DESC="Initialize cloud-init data source."
        ;;

    cloud-init-config)
        CMD="modules --mode=config"
        DESC="Execute cloud-init modules."
        ;;

    cloud-init-final)
        CMD="modules --mode=final"
        DESC="Post-boot cloud-init actions."
        ;;

    *)
        echo "Unknown step $step"
        exit 1
        ;;

    esac

    ##
    ## Create the script. Things get meta here: when you see a \$, that's a
    ## literal dollar sign that will get expanded when running the init script.
    ## When you see a $, that's a variable that gets expanded now.
    ##

    cat > "$PACKAGE_DIRECTORY/etc/init.d/$step" <<_EOS
#! /bin/sh
set -e

# /etc/init.d/$step: $DESC

CLOUD_INIT=/usr/bin/cloud-init

[ -f /etc/init.d/init-functions ] && . /etc/init.d/init-functions

if test -f /etc/default/$step; then
    . /etc/default/$step
fi

export PATH="\${PATH:+\$PATH:}/usr/sbin:/sbin"

case "\$1" in
    start)
        log_daemon_msg "Running $step" || true
        if \$CLOUD_INIT $CMD \$CLOUD_INIT_ARGS ; then
            log_end_msg 0 || true

        else
            log_end_msg 1 || true

        fi
        ;;

    stop)
        ;;

    *)
        log_action_msg "Usage: /etc/init.d/$step {start|stop|}"
        exit 1
        ;;

esac

exit 0
_EOS

    chmod +x "$PACKAGE_DIRECTORY/etc/init.d/$step"
done

##
## Create a pre-remove script.
##

cat > "$PACKAGE_DIRECTORY/CONTROL/prerm" <<"_EOS"
#! /bin/sh
if test "x$D" != "x"; then
    exit 1
else
    if test "$PKG_ROOT" != "/"; then
        export SYSROOT=$PKG_ROOT
    fi

    for step in cloud-init-local cloud-init cloud-init-config cloud-init-final;
    do
        update-rc.d -f $step remove
        rm -f $SYSROOT/etc/init.d/$step
    done
fi
_EOS

chmod +x "$PACKAGE_DIRECTORY/CONTROL/prerm"

##
## Create a post-install script.
##

cat > "$PACKAGE_DIRECTORY/CONTROL/postinst" <<"_EOS"
#! /bin/sh

set -x
if test "$PKG_ROOT" != "/"; then
    export SYSROOT=$PKG_ROOT
fi

if test "x$D" != "x"; then
    exit 1
fi

/usr/sbin/update-rc.d cloud-init-local defaults 10
/usr/sbin/update-rc.d cloud-init defaults 15
/usr/sbin/update-rc.d cloud-init-config defaults 16
/usr/sbin/update-rc.d cloud-init-final defaults 30
_EOS

chmod +x "$PACKAGE_DIRECTORY/CONTROL/postinst"

##
## TODO: Express the other Python package dependencies and create them as
## packages too. See requirements.txt in the source of this package.
##

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: python-cloud-init
Depends: python2
Priority: optional
Version: 0.7.6
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://github.com/openstack/cloud-init/archive/0.7.6.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: cloud initialization infrastructure.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

