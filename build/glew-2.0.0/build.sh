##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the OpenGL GLEW package.
##
## Author:
##
##     Evan Green 26-Apr-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh
. ../x_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency glproto_1.4.17
    extract_dependency xproto_7.0.31
    extract_dependency kbproto_1.0.7
    extract_dependency xextproto_7.3.0
    extract_dependency libxau_1.0.8
    extract_dependency libx11_1.6.5
    extract_dependency libxext_1.3.3
    extract_dependency libxcb_1.12
    extract_dependency dri2proto_2.8
    extract_dependency libxdamage_1.1.4
    extract_dependency libxfixes_5.0.3
    extract_dependency fixesproto_5.0
    extract_dependency damageproto_1.2.1
    extract_dependency expat_2.1.0
    extract_dependency libxdmcp_1.1.2
    extract_dependency mesa_17.0.0

    printf "Copying source to build directory..."
    cp -Rp ${SOURCE_DIRECTORY}/* .
    echo "done"
    ;;

  build)
    export SYSTEM=minoca
    export AR=${TARGET}-ar
    export STRIP=${TARGET}-strip
    mkdir -p build
    args="CC=${TARGET}-gcc LD=${TARGET}-gcc DIST_DIR=$PWD/build \
 LDFLAGS.EXTRA=-Wl,-L$DEPENDROOT/usr/lib,-rpath-link=$DEPENDROOT/usr/lib"

    $MAKE $PARALLEL_MAKE $args
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY" $args
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

