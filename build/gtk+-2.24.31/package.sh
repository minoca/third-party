##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the GTK+ framework.
##
## Author:
##
##     Evan Green 19-Apr-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

##
## Avoid trailing slashes in the cp commands, as cp on Darwin copies the
## contents of the directory (rather than the directory itself) if there are
## trailing slashes on either the source or the destination.
##

rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/usr"
rm -f $BUILD_DIRECTORY/usr/lib/*.la
rm -f $BUILD_DIRECTORY/usr/lib/gtk-2.0/2.10.0/engines/*.la
rm -f $BUILD_DIRECTORY/usr/lib/gtk-2.0/2.10.0/immodules/*.la
rm -f $BUILD_DIRECTORY/usr/lib/gtk-2.0/2.10.0/printbackends/*.la
rm -f $BUILD_DIRECTORY/usr/lib/gtk-2.0/modules/*.la
rm -rf $BUILD_DIRECTORY/usr/share/man

cp -Rpv "$BUILD_DIRECTORY/usr" "$PACKAGE_DIRECTORY"
cp -Rpv "$BUILD_DIRECTORY/etc" "$PACKAGE_DIRECTORY"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: gtk
Depends: libatk, gdk-pixbuf, libpango, libx11, libxinerama, libxi, libcairo, libpixman, tiff, libgcc
Priority: optional
Version: 2.36.6
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://ftp.gnome.org/pub/gnome/sources/gtk+/2.24/gtk+-2.24.31.tar.xz
Installed-Size: $INSTALLED_SIZE
Description: Framework used for creating graphical user interfaces.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

