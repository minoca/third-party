##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the dropbear package.
##
## Author:
##
##     Evan Green 3-Aug-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBZ=libz_1.2.11

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency "$LIBZ"
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include -I$BUILD_DIRECTORY \
-I$SOURCE_DIRECTORY/libtommath"

    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE MULTI=1 \
        PROGRAMS="dropbear dbclient dropbearkey dropbearconvert scp"

    mkdir -p "$OUTPUT_DIRECTORY/usr/bin" "$OUTPUT_DIRECTORY/usr/sbin"
    cp -pv dropbearmulti "$OUTPUT_DIRECTORY/usr/sbin/dropbear"
    cd "$OUTPUT_DIRECTORY/usr/bin"
    ln -svf ../sbin/dropbear dbclient
    ln -svf ../sbin/dropbear dropbearkey
    ln -svf ../sbin/dropbear dropbearconvert
    ln -svf ../sbin/dropbear scp
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

