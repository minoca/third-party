##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the irssi package.
##
## Author:
##
##     Vasco Costa 11-Nov-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBGCC=libgcc_6.3.0
GETTEXT=gettext_0.19.8.1
LIBICONV=libiconv_1.14
PCRE=libpcre_8.39
GLIB=libglib_2.49.6
OPENSSL=libopenssl_1.0.2h
NCURSES=libncurses_5.9

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$LIBGCC"
    extract_dependency "$GETTEXT"
    extract_dependency "$LIBICONV"
    extract_dependency "$PCRE"
    extract_dependency "$GLIB"
    extract_dependency "$OPENSSL"
    extract_dependency "$NCURSES"
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include \
 -I$DEPENDROOT/usr/include/glib-2.0 -I$DEPENDROOT/usr/lib/glib-2.0/include"

    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    if [ "$BUILD_OS" = "minoca" ]; then
        export LD_LIBRARY_PATH=$DEPENDROOT/usr/lib
    fi

    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --with-perl=no \
                                     --sysconfdir=/etc \
                                     --disable-static \
                                     --prefix="/usr" \
                                     --program-prefix= \
                                     CFLAGS="$CFLAGS" \
                                     CPPFLAGS="$CPPFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install-strip DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

