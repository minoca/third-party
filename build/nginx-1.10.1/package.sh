##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the nginx http server.
##
## Author:
##
##     Evan Green 22-Jun-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/etc" "$PACKAGE_DIRECTORY"
cp -Rpv "$BUILD_DIRECTORY/usr/sbin" "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/var/log/nginx"
mkdir -p "$PACKAGE_DIRECTORY/var/run"
mkdir -p "$PACKAGE_DIRECTORY/var/www"
cp -Rpv "$BUILD_DIRECTORY/usr/html" "$PACKAGE_DIRECTORY/var/www"

##
## Create the init script
##

mkdir -p "$PACKAGE_DIRECTORY/etc/init.d"
cat > "$PACKAGE_DIRECTORY/etc/init.d/nginx" <<_EOS
#! /bin/sh
set -e

# /etc/init.d/nginx: start and stop the nginx web server.

NGINX=/usr/sbin/nginx
LOGDIR=/var/log/nginx
PIDDIR=/var/run
PIDFILE="\$PIDDIR/nginx.pid"

test -x \$NGINX || exit 0
( \$NGINX -V 2>&1 | grep -q nginx ) 2>/dev/null || exit 0

[ -f /etc/init.d/init-functions ] && . /etc/init.d/init-functions

umask 022
[ -f /etc/default/nginx ] && . /etc/default/nginx
[ -n "\$2" ] && NGINX_OPTS="\$NGINX_OPTS \$2"

[ -d "\$LOGDIR" ] || mkdir -p "\$LOGDIR"
[ -d "\$PIDDIR" ] || mkdir -p "\$PIDDIR"

export PATH="\${PATH:+\$PATH:}/usr/sbin:/sbin"

case "\$1" in
    start)
        log_daemon_msg "Starting nginx" || true
        if \$NGINX \$NINGX_OPTS; then
            log_end_msg 0 || true

        else
            log_end_msg 1 || true

        fi
        ;;

    stop)
        log_daemon_msg "Stopping nginx" || true
        if start-stop-daemon -K -p "\$PIDFILE" -qo -n nginx; then
            log_end_msg 0 || true

        else
            log_end_msg 1 || true
        fi
        ;;

    restart|force-reload)
        \$0 stop
        \$0 start
        ;;

    configtest)
        \$NGINX -t
        ;;

    *)
        log_action_msg \
             "Usage: \$0 {start|stop|restart|force-reload|configtest}"

        exit 1
        ;;

esac

exit 0
_EOS
chmod +x "$PACKAGE_DIRECTORY/etc/init.d/nginx"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: nginx
Depends: libpcre, libgcc, libz, libopenssl
Priority: optional
Version: 1.10.1
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://nginx.org/download/nginx-1.10.1.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Nginx web server.
_EOS

cat > "$PACKAGE_DIRECTORY/CONTROL/conffiles" <<_EOS
/var/www/html/index.html
/var/www/html/50x.html
/etc/nginx/koi-win
/etc/nginx/koi-utf
/etc/nginx/win-utf
/etc/nginx/mime.types
/etc/nginx/fastcgi_params
/etc/nginx/fastcgi.conf
/etc/nginx/uwsgi_params
/etc/nginx/scgi_params
/etc/nginx/nginx.conf
_EOS

cat > "$PACKAGE_DIRECTORY/CONTROL/prerm" <<"_EOS"
#! /bin/sh
if test "x$D" != "x"; then
    exit 1
else
    /etc/init.d/nginx stop
    update-rc.d -f nginx remove
    rm -f /etc/init.d/nginx
fi
_EOS

chmod +x "$PACKAGE_DIRECTORY/CONTROL/prerm"

cat > "$PACKAGE_DIRECTORY/CONTROL/postinst" <<"_EOS"
#! /bin/sh

ROOTLINE=
CHROOTCMD=
if test "$PKG_ROOT" != "/"; then
    ROOTLINE="-R$PKG_ROOT"
    CHROOTCMD="chroot $PKG_ROOT -- "
fi

if test "x$D" != "x"; then
    exit 1

else
    useradd $ROOTLINE --system --home=/var/www --no-create-home \
        --shell=/bin/false www-data || true

    $CHROOTCMD /usr/sbin/update-rc.d nginx defaults
fi
_EOS

chmod +x "$PACKAGE_DIRECTORY/CONTROL/postinst"

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"


