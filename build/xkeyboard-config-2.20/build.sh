##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes xkeyboard-config-2.20
##
## Author:
##
##     Evan Green 4-Apr-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh
. ../x_common.sh

export PATH=$PATH:$DEPENDROOT/usr/bin
export LD_LIBRARY_PATH=$DEPENDROOT/usr/lib

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency xproto_7.0.31
    extract_dependency util-macros_1.19.1
    extract_dependency intltool_0.50.2
    extract_dependency gettext_0.19.8.1

    extract_dependency expat_2.1.0
    extract_dependency libunistring_0.9.6
    extract_dependency libiconv_1.14

    sh ${SOURCE_DIRECTORY}/configure $XORG_CONFIG \
                                     --disable-runtime-deps \
                                     --with-xkb-rules-symlink=xorg

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

