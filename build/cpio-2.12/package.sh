##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages cpio.
##
## Author:
##
##     Vasco Costa 20-Nov-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/bin" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: cpio
Depends: libgcc, libiconv, gettext
Priority: optional
Version: 2.12
Architecture: $PACKAGE_ARCH
Maintainer: Vasco Costa <gluon81@gmx.com>
Section: main
Source: http://ftp.gnu.org/pub/gnu/cpio/cpio-2.12.tar.bz2
Installed-Size: $INSTALLED_SIZE
Description: GNU cpio is a tool for creating archives, or copying files from one place to another.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

