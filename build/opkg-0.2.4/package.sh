##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the opkg utility.
##
## Author:
##
##     Evan Green 16-Feb-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/bin" "$PACKAGE_DIRECTORY/usr"
mv "$PACKAGE_DIRECTORY/usr/bin/opkg-cl" "$PACKAGE_DIRECTORY/usr/bin/opkg"
cp -Rpv "$BUILD_DIRECTORY/lib" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/include" "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/usr/lib/opkg"

##
## Libtool sucks.
##

rm "$PACKAGE_DIRECTORY/usr/lib/libopkg.la"

##
## Create a Minoca and architecture specific configuration file.
##

parch=${PACKAGE_ARCH#minoca-}
BINROOT="$SRCROOT/$ARCH$VARIANT$DEBUG/bin"
SYSTEM_VERSION=`cat $BINROOT/kernel-version | sed 's/\([0-9]*\)\.\([0-9]*\)\..*/\1.\2/'`
mkdir -p "$PACKAGE_DIRECTORY/etc/opkg"
cat > "$PACKAGE_DIRECTORY/etc/opkg/opkg.conf" <<_EOS
arch $PACKAGE_ARCH 100
arch all 200
src/gz main http://www.minocacorp.com/packages/$SYSTEM_VERSION/$parch/main
dest root /
dest ram /tmp
dest mnt /mnt
lists_dir ext /var/opkg-lists
_EOS

trim_cr "$PACKAGE_DIRECTORY/etc/opkg/opkg.conf"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: opkg
Depends: wget, tar, gzip
Priority: required
Version: 0.2.4
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://downloads.yoctoproject.org/releases/opkg/opkg-0.2.4.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: opkg is a lightweight package management system.
_EOS

cat > "$PACKAGE_DIRECTORY/CONTROL/conffiles" <<_EOS
/etc/opkg/opkg.conf
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

