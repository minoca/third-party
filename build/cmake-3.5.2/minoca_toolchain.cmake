##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     minoca_toolchain.cmake
##
## Abstract:
##
##     This CMake file contains the toolchain information for cross-compiling
##     CMake for the Minoca Operating System.
##
## Author:
##
##     Chris Stevens 22-Jun-2016
##
## Environment:
##
##     Build
##

##
## Set variables to indicate that this is a cross compile.
##

SET(CMAKE_CROSSCOMPILING 1)
SET(CMAKE_SYSTEM_NAME Minoca)
SET(CMAKE_HOST_SYSTEM `uname`)
SET(CMAKE_SYSTEM_PROCESSOR $ENV{ARCH})

##
## Set the cross compilers and associated flags.
##

SET(CMAKE_C_COMPILER $ENV{TARGET}-gcc)
SET(CMAKE_CXX_COMPILER $ENV{TARGET}-g++)
SET(CMAKE_C_FLAGS $ENV{CFLAGS})
SET(CMAKE_CXX_FLAGS $ENV{CXXFLAGS})

##
## Set the depend root as the preferred path search location during
## cross-compilation. If there are any libraries or includes specific to the
## target, they will be extracted to the depend root. But, as they may not be
## present, set searches to BOTH, so that the raw supplied paths are also
## searched.
##

SET(CMAKE_FIND_ROOT_PATH $ENV{DEPENDROOT})
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY BOTH)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE BOTH)

##
## Set the results for the TRYRUN tests that can only be run on the target
## system. Large File Support (LFS), POSIX sterror_r, and poll fine are
## supported. GLIBC's sterror_r is not.
##

SET(KWSYS_LFS_WORKS 0)
SET(HAVE_POSIX_STRERROR_R 0)
SET(HAVE_POSIX_STRERROR_R__TRYRUN_OUTPUT "")
SET(HAVE_POLL_FINE_EXITCODE 0)
SET(HAVE_GLIBC_STRERROR_R 1)
SET(HAVE_GLIBC_STRERROR_R__TRYRUN_OUTPUT "")
