##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the cmake package.
##
## Author:
##
##     Chris Stevens 22-Jun-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

NCURSES=libncurses_5.9

##
## Export the flags so that they can get picked up by either the toolchain
## file or the bootstrap script.
##

export CFLAGS="$CFLAGS"
export CXXFLAGS="$CFLAGS"
export LDFLAGS="$LDFLAGS"

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure-tools)
    export CFLAGS="$CFLAGS -I${OUTPUT_DIRECTORY}/build/include"
    export CXXFLAGS="$CFLAGS"
    export LDFLAGS="$LDFLAGS -L$OUTPUT_DIRECTORY/build/lib"
    sh ${SOURCE_DIRECTORY}/bootstrap --prefix="/"
    ;;

  configure)
    extract_dependency "$NCURSES"
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    export CXXFLAGS="$CFLAGS"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib"

    ##
    ## When cross-compiling, assume that the tools have already been built and
    ## use the version of cmake that runs on the build machine.
    ##

    if test "x$BUILD_OS" != "xminoca"; then
        cmake -G "Unix Makefiles" \
              -D CMAKE_INSTALL_PREFIX="/usr" \
              -D CMAKE_TOOLCHAIN_FILE="$BUILD_DIRECTORY/minoca_toolchain.cmake" \
              ${SOURCE_DIRECTORY}

    ##
    ## Otherwise just bootstrap a native version of cmake, rather than relying
    ## on a cross-compiled version.
    ##

    else
        if [ "$PARALLEL_MAKE" ]; then
            CMAKE_PARALLEL=--parallel=`echo $PARALLEL_MAKE | sed 's/-j//'`
        fi

        sh ${SOURCE_DIRECTORY}/bootstrap $CMAKE_PARALLEL --prefix="/usr"
    fi

    ;;

  build-tools)
    $MAKE $PARALLEL_MAKE COLOR=0
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY" COLOR=0
    ;;

  build)
    $MAKE $PARALLEL_MAKE COLOR=0
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY" COLOR=0
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

