##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the croco library.
##
## Author:
##
##     Evan Green 31-Aug-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

rm "$BUILD_DIRECTORY"/usr/lib/*.la
mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libcroco
Depends: libglib libxml2, libgcc
Priority: optional
Version: 0.6.11
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://ftp.gnome.org/pub/GNOME/sources/libcroco/0.6/libcroco-0.6.11.tar.xz
Installed-Size: $INSTALLED_SIZE
Description: libcroco CSS2 parsing library.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

