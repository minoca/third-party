##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the GPGME library.
##
## Author:
##
##     Evan Green 2-Sep-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

rm "$BUILD_DIRECTORY"/usr/lib/*.la
rm -rf "$BUILD_DIRECTORY"/usr/share/info

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/share" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libgpgme
Depends: libgpg-error, libassuan, libgcc
Priority: optional
Version: 1.6.0
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: ftp://ftp.gnupg.org/gcrypt/gpgme/gpgme-1.6.0.tar.bz2
Installed-Size: $INSTALLED_SIZE
Description: GPG application integration library.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

