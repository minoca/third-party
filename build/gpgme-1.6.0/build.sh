##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the gpgme package.
##
## Author:
##
##     Evan Green 2-Sep-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBGPG_ERROR=libgpg-error_1.24
LIBASSUAN=libassuan_2.4.3

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$LIBGPG_ERROR"
    extract_dependency "$LIBASSUAN"

    export CPPFLAGS="$CPPFLAGS -I$DEPENDROOT/usr/include \
 -I$DEPENDROOT/usr/include/p11-kit-1"

    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --disable-static \
                                     --prefix="/usr" \
                                     --with-libgpg-error-prefix="$DEPENDROOT/usr" \
                                     --with-libassuan-prefix="$DEPENDROOT/usr" \
                                     CFLAGS="$CFLAGS" \
                                     CPPFLAGS="$CPPFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

