##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the samba package.
##
## Author:
##
##     Evan Green 13-Apr-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh

cd $BUILD_DIRECTORY
CONFIG_ARGS="--without-systemd \
 --without-acl-support \
 --with-pie \
 --prefix=/usr \
 --destdir=$OUTPUT_DIRECTORY \
 --sysconfdir=/etc \
 --localstatedir=/var \
 --with-pammodulesdir=/lib/security \
 --disable-rpath \
 --hostcc=$CC \
 --enable-fhs \
"

export PYTHON=$DEPENDROOT/usr/bin/python
export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"

export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

export LD_LIBRARY_PATH=$DEPENDROOT/usr/lib
export PYTHONPATH=$DEPENDROOT/usr/lib/python2.7
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency python2_2.7.11
    extract_dependency gnutls_3.5.3
    extract_dependency libiconv_1.14
    extract_dependency gettext_0.19.8.1
    extract_dependency openldap_2.4.44

    extract_dependency libnettle_3.2
    extract_dependency libtasn1_4.9
    extract_dependency libidn_1.33
    extract_dependency p11kit_0.23.2
    extract_dependency libopenssl_1.0.2h
    extract_dependency expat_2.1.0
    extract_dependency libffi_3.2.1

    ##
    ## Copy the source directory to the build directory, since
    ## building out of tree seems to be unsupported.
    ##

    printf "Copying source to build directory..."
    cp -Rp $SOURCE_DIRECTORY/* .
    echo "Done"
    python buildtools/bin/waf configure $CONFIG_ARGS
    ;;

  build)

    ##
    ## The build cannot be parallelized because Python 2.7's subprocess
    ## module may deadlock, since it forks with threads, and other threads
    ## might be holding the heap lock.
    ##

    python buildtools/bin/waf build -j1
    python buildtools/bin/waf install -j1 --destdir="$OUTPUT_DIRECTORY"
    cp examples/smb.conf.default $OUTPUT_DIRECTORY/etc/samba
    mkdir -p $OUTPUT_DIRECTORY/etc/openldap/schema
    cp -p examples/LDAP/* $OUTPUT_DIRECTORY/etc/openldap/schema
    mv $OUTPUT_DIRECTORY/etc/openldap/schema/README \
       $OUTPUT_DIRECTORY/etc/openldap/schema/README.LDAP

    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

