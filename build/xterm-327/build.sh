##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes xterm-327
##
## Author:
##
##     Evan Green 7-Apr-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh
. ../x_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency xproto_7.0.31
    extract_dependency util-macros_1.19.1
    extract_dependency xorg-server_1.19.3
    extract_dependency fontsproto_2.1.3
    extract_dependency randrproto_1.5.0
    extract_dependency renderproto_0.11.1
    extract_dependency videoproto_2.3.3
    extract_dependency libncurses_5.9
    extract_dependency libxinerama_1.1.3
    extract_dependency libx11_1.6.5
    extract_dependency libxt_1.1.5
    extract_dependency libice_1.0.9
    extract_dependency libxmu_1.1.2
    extract_dependency libxaw_1.0.13
    extract_dependency libxpm_3.5.12
    extract_dependency libxcursor_1.1.14
    extract_dependency fontconfig_2.12.1
    extract_dependency libxft_2.3.2

    extract_dependency kbproto_1.0.7
    extract_dependency xextproto_7.3.0
    extract_dependency fixesproto_5.0
    extract_dependency damageproto_1.2.1
    extract_dependency xcmiscproto_1.2.2
    extract_dependency xtrans_1.3.5
    extract_dependency bigreqsproto_1.1.2
    extract_dependency randrproto_1.5.0
    extract_dependency renderproto_0.11.1
    extract_dependency inputproto_2.3.2
    extract_dependency fontsproto_2.1.3
    extract_dependency videoproto_2.3.3
    extract_dependency compositeproto_0.4.2
    extract_dependency recordproto_1.14.2
    extract_dependency scrnsaverproto_1.2.2
    extract_dependency resourceproto_1.2.0
    extract_dependency presentproto_1.1
    extract_dependency xineramaproto_1.2.1
    extract_dependency libxkbfile_1.0.9
    extract_dependency libxfont2_2.0.1
    extract_dependency libpixman_0.34.0
    extract_dependency libsm_1.2.2
    extract_dependency libxfixes_5.0.3
    extract_dependency fixesproto_5.0

    export TERMINFO=/usr/share/terminfo
    sh ${SOURCE_DIRECTORY}/configure $XORG_CONFIG \
                                     --with-app-defaults=/etc/X11/app-defaults
    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

