##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     update.sh
##
## Abstract:
##
##     This script updates the source package based on a new certdata.txt file.
##
## Author:
##
##     Evan Green 28-Mar-2016
##
## Environment:
##
##     Build
##

set -e

CERTDATA=certdata.txt

if ! [ -r $CERTDATA ]; then
    echo "Error: cannot find certdata.txt."
    exit 1
fi

mkdir -p ca-certificates
date "+%Y%m%d" > ca-certificates/revision
cp $CERTDATA make-ca.sh make-cert.pl ca-certificates/
tar -czf $SRCROOT/third-party/src/ca-certificates.tar.gz ./ca-certificates
rm -rf ./ca-certificates

