##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the ca-certificates.
##
## Author:
##
##     Evan Green 28-Mar-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY"
cp -Rpv "$BUILD_DIRECTORY/etc" "$PACKAGE_DIRECTORY"
REVISION=`cat $BUILD_DIRECTORY/revision`

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: ca-certificates
Priority: optional
Version: $REVISION
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://hg.mozilla.org/releases/mozilla-release/raw-file/default/security/nss/lib/ckfw/builtins/certdata.txt
Installed-Size: $INSTALLED_SIZE
Description: Root Certificates from Mozilla.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

