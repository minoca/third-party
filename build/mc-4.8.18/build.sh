##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the mc package.
##
## Author:
##
##     Vasco Costa 23-Nov-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

GLIB=libglib_2.49.6
NCURSES=libncurses_5.9
PCRE=libpcre_8.39

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$GLIB"
    extract_dependency "$NCURSES"
    extract_dependency "$PCRE"
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include \
 -I$DEPENDROOT/usr/include/glib-2.0 -I$DEPENDROOT/usr/lib/glib-2.0/include"

    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     --sysconfdir=/etc \
                                     --program-prefix= \
                                     --disable-vfs \
                                     --with-screen=ncurses \
                                     CFLAGS="$CFLAGS" \
                                     CPPFLAGS="$CPPFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install-strip DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

