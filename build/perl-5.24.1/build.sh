##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the Perl package.
##
## Author:
##
##     Evan Green 8-Jan-2015
##
## Environment:
##
##     Build
##

. ../build_common.sh

##
## Presumably because they've reinvented all of autoconf but shakier, Perl
## seems to need to build out of its source directory. The source directory
## will need to be copied wholesale into the build directory.
##

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    if test "x$BUILD_OS" != "xminoca"; then
        echo "Error: Perl cannot be cross compiled."
        exit 3
    fi

    echo "Copying source to $PWD"
    cp -Rpf ${SOURCE_DIRECTORY}/* ${SOURCE_DIRECTORY}/.dir-locals.el .
    sh ./Configure -Dprefix="/usr" \
                   -Dcc=$TARGET-gcc \
                   -Accflags="$CFLAGS -fwrapv -fno-strict-aliasing -fno-stack-protector" \
                   -Duse64bitint=y \
                   -Dusemymalloc=n \
                   -Dusenm=n \
                   -Duseshrplib=y \
                   -Dusethreads=y \
                   -Amyhostname=minoca \
                   -Amydomain=minocacorp.com \
                   -Acf_email=info@minocacorp.com \
                   -Aperladmin=info@minocacorp.com \
                   -Alddlflags=-shared \
                   -Astatic_ext=Hash/Util \
                   -d -e

    ;;

  configure-tools)
    echo "Copying source to $PWD"
    cp -Rpf ${SOURCE_DIRECTORY}/* ${SOURCE_DIRECTORY}/.dir-locals.el .
    if test "x$BUILD_OS" = "xwin32"; then
      OUTPUT_DIRECTORY=`echo $OUTPUT_DIRECTORY | sed 's|/|\\\\\\\\|g'`
      INSTDRIVE=`echo $SRCROOT | sed 's|\([a-zA-Z]:\).*|\1|'`
      tab=`printf '\t'`
      if ! [ -f ./win32/makefile.mk.orig ]; then
        cp -pv ./win32/makefile.mk ./win32/makefile.mk.orig
        sed -e "s|^INST_DRV${tab}.*|INST_DRV *= $INSTDRIVE|" \
            -e "s|^INST_TOP${tab}.*|INST_TOP *= $OUTPUT_DIRECTORY|" \
            -e "s|^#WIN64${tab}.*|WIN64 *= undef|" \
            -e "s|^CCHOME${tab}.*|CCHOME *= $SRCROOT/tools/win32/MinGW|" \
            -e "s|^EMAIL${tab}.*|EMAIL *= info@minocacorp.com|" \
            ./win32/makefile.mk.orig > ./win32/makefile.mk

      fi

    else
      if [ "$BUILD_OS" = "minoca" ]; then
        EXTRA_ARGS="-Duseshrplib=y -Alddlflags=-shared -Astatic_ext=Hash/Util"

      elif [ "$BUILD_OS" = "cygwin" ]; then
        EXTRA_ARGS="-Duseshrplib=y"

      elif [ "$BUILD_OS" = "freebsd" ]; then
        EXTRA_ARGS="-Duserelocatableinc=y -Dmake=$MAKE"

      else
        EXTRA_ARGS="-Duserelocatableinc=y"
      fi

      sh ./Configure -Dprefix="$OUTPUT_DIRECTORY" \
                     -Duse64bitint=y \
                     -Dusemymalloc=n \
                     -Dusenm=n \
                     -Dusethreads=y \
                     -Amyhostname=minoca \
                     -Amydomain=minocacorp.com \
                     -Acf_email=info@minocacorp.com \
                     -Aperladmin=info@minocacorp.com \
                     $EXTRA_ARGS \
                     -d -e
    fi

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    OLDPWD=$PWD
    cd "$OUTPUT_DIRECTORY/usr/bin"
    chmod +x perl perlthanks pstruct
    cd "$OUTPUT_DIRECTORY/usr/lib"
    ln -sf ./perl5/5.24.1/`uname -m`-minoca-thread-multi-64int/CORE/libperl.so ./libperl.so
    cd "$OLDPWD"
    ;;

  test)
    LD_LIBRARY_PATH=`pwd` ./runtests choose
    ;;

  build-tools)
    if test "x$BUILD_OS" = "xwin32"; then
      export DMAKEROOT="$OUTPUT_DIRECTORY/share/startup"
      cd ./win32
      unset SHELL
      unset MAKE
      dmake -f makefile.mk
      dmake -f makefile.mk install

    else
      $MAKE $PARALLEL_MAKE
      $MAKE $PARALLEL_MAKE install
      chmod +x "$OUTPUT_DIRECTORY/bin/perl"
    fi
  ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac
