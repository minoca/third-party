##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the expect package.
##
## Author:
##
##     Evan Green 20-May-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBGCC=libgcc_6.3.0
ZLIB=libz_1.2.11
TCL=tcl_8.6.5

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency "$LIBGCC" || echo "Ignoring libgcc."
    extract_dependency "$ZLIB"
    extract_dependency "$TCL"
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     --with-tcl="$DEPENDROOT/usr/lib" \
                                     --enable-shared \
                                     CFLAGS="$CFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

