##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the MySQL utility in separate client and server
##     packages.
##
## Author:
##
##     Chris Stevens 22-Jun-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

##
## The MySQL 5.7.13 Community Server source builds the binaries for the
## mysql-client and mysql-server packages. Create them both here.
##

CLIENT_DIRECTORY="$PACKAGE_DIRECTORY/client"
SERVER_DIRECTORY="$PACKAGE_DIRECTORY/server"

##
## Make the client package.
##

##
## Copy the client libraries, ignoring the plugins as those go in the server
## package.
##

CLIENT_LIBRARIES="libmysqlclient.so.20.3.0
libmysqlclient.so
libmysqlclient.so.20
libmysqlclient.a
libmysqld.a
libmysqlservices.a
pkgconfig"

mkdir -p "$CLIENT_DIRECTORY/usr/lib"
for file in $CLIENT_LIBRARIES; do
    file="$BUILD_DIRECTORY/usr/lib/$file"
    if test -e "$file"; then
        cp -Rpv $file "$CLIENT_DIRECTORY/usr/lib"
    fi
done

##
## Copy all of the headers.
##

mkdir -p "$CLIENT_DIRECTORY/usr/include/mysql"
cp -Rpv "$BUILD_DIRECTORY/usr/include"/* "$CLIENT_DIRECTORY/usr/include/mysql"

##
## Copy the client binaries. The remainder of the binaries are included in the
## server package.
##

CLIENT_BINARIES="innochecksum
lz4_decompress
myisam_ftdump
mysql
mysql_client_test
mysql_client_test_embedded
mysql_config
mysql_config_editor
mysql_embedded
mysql_plugin
mysqladmin
mysqlcheck
mysqldump
mysqldumpslow
mysqlimport
mysqlpump
mysqlshow
zlib_decompress
"

mkdir -p "$CLIENT_DIRECTORY/usr/bin"
for file in $CLIENT_BINARIES; do
    file="$BUILD_DIRECTORY/usr/bin/$file"
    if test -e "$file"; then
        cp -pv $file "$CLIENT_DIRECTORY/usr/bin"
    fi
done

mkdir -p "$CLIENT_DIRECTORY/CONTROL"
INSTALLED_SIZE=`compute_size $CLIENT_DIRECTORY`
cat > "$CLIENT_DIRECTORY/CONTROL/control" <<_EOS
Package: mysql-client
Depends: libncurses
Priority: optional
Version: 5.7.13
Architecture: $PACKAGE_ARCH
Maintainer: Chris Stevens <chris@minocacorp.com>
Section: main
Source: http://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-5.7.13.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: The MySQL database client.
_EOS

create_package "$CLIENT_DIRECTORY" "$BUILD_DIRECTORY"

##
## Make the server package.
##

##
## Copy all of the plugin libraries.
##

mkdir -p "$SERVER_DIRECTORY/usr/lib/mysql"
cp -Rpv "$BUILD_DIRECTORY/usr/lib/plugin" "$SERVER_DIRECTORY/usr/lib/mysql"

##
## Copy the support files. These are helpful for firing up the server.
##

mkdir -p "$SERVER_DIRECTORY/usr/mysql"
cp -Rpv "$BUILD_DIRECTORY/usr/support-files" "$SERVER_DIRECTORY/usr/mysql"

##
## Error messages are included in the share folder.
##

mkdir -p "$SERVER_DIRECTORY/usr/share/mysql"
cp -Rpv "$BUILD_DIRECTORY/usr/share"/* "$SERVER_DIRECTORY/usr/share/mysql"

##
## Only copy some of the binaries, otherwise they will collide with the client
## package.
##

SERVER_BINARIES="my_print_defaults
myisamchk
myisamlog
myisampack
mysql_install_db
mysql_secure_installation
mysql_ssl_rsa_setup
mysql_tzinfo_to_sql
mysql_upgrade
mysqlbinlog
mysqld
mysqld_multi
mysqld_safe
mysqlslap
mysqltest
mysqltest_embedded
mysqlxtest
perror
replace
resolve_stack_dump
resolveip
"

mkdir -p "$SERVER_DIRECTORY/usr/bin"
for file in $SERVER_BINARIES; do
    file="$BUILD_DIRECTORY/usr/bin/$file"
    if test -e "$file"; then
        cp -pv $file "$SERVER_DIRECTORY/usr/bin"
    fi
done

mkdir -p "$SERVER_DIRECTORY/CONTROL"
INSTALLED_SIZE=`compute_size $SERVER_DIRECTORY`
cat > "$SERVER_DIRECTORY/CONTROL/control" <<_EOS
Package: mysql-server
Depends: mysql-client
Priority: optional
Version: 5.7.13
Architecture: $PACKAGE_ARCH
Maintainer: Chris Stevens <chris@minocacorp.com>
Section: main
Source: http://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-5.7.13.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: The MySQL database server.
_EOS

create_package "$SERVER_DIRECTORY" "$BUILD_DIRECTORY"

