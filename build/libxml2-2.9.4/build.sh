##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the XML library.
##
## Author:
##
##     Evan Green 20-Jun-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

ZLIB=libz_1.2.11
LIBICONV=libiconv_1.14
READLINE=libreadline_6.3

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$ZLIB"
    extract_dependency "$LIBICONV"
    extract_dependency "$READLINE"
    export CFLAGS="$CFLAGS"
    export CPPFLAGS="-I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export Z_LIBS="-lz"
    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --with-history \
                                     --disable-static \
                                     --prefix="/usr" \
                                     --without-python \
                                     CFLAGS="$CFLAGS" \
                                     CPPFLAGS="$CPPFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install-strip DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

