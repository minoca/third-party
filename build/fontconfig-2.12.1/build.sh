##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the libXau package.
##
## Author:
##
##     Evan Green 22-Mar-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBGCC=libgcc_6.3.0

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency libfreetype_2.7.1
    extract_dependency expat_2.1.0
    extract_dependency "$LIBGCC" || echo "Ignoring libgcc."

    extract_dependency libharfbuzz_1.4.4
    extract_dependency libglib_2.49.6

    export CPPFLAGS="$CPPFLAGS -I$DEPENDROOT/usr/include \
 -I$DEPENDROOT/usr/include/freetype2"

    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     --sysconfdir=/etc \
                                     --localstatedir=/var \
                                     --disable-docs \
                                     --docdir=/usr/share/doc/fontconfig-2.12.1 \
                                     CFLAGS="$CFLAGS" \
                                     CPPFLAGS="$CPPFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

