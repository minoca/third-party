##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the freetype library.
##
## Author:
##
##     Evan Green 7-Mar-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh

BZIP2="bzip2_1.0.6"
ZLIB="libz_1.2.11"
LIBPNG="libpng_1.6.28"

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$BZIP2" && export BZIP2_LIBS=-lbz2 BZIP2_CFLAGS=" "
    extract_dependency "$ZLIB" && export ZLIB_LIBS=-lz ZLIB_CFLAGS=" "
    extract_dependency "$LIBPNG" && export LIBPNG_LIBS=-lpng LIBPNG_CFLAGS=" "

    ##
    ## This library really needs to be built twice: once before harfbuzz is
    ## built, and then again once it is.
    ##

    if extract_dependency libharfbuzz_1.4.4; then
        echo "Successfully extracted Harfbuzz"
        extract_dependency libglib_2.49.6
        extract_dependency libpcre_8.39
        extract_dependency gettext_0.19.8.1
        extract_dependency libiconv_1.14

        WITH_HARFBUZZ=--with-harfbuzz=yes
        echo "yes" > $BUILD_DIRECTORY/harfbuzz-status

    else
        echo "Building without Harfbuzz."
        echo "Remember to rebuild freetype once Harfbuzz is built."
        WITH_HARFBUZZ=--with-harfbuzz=no
        echo "no" > $BUILD_DIRECTORY/harfbuzz-status
    fi

    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include \
 -I$DEPENDROOT/usr/include/glib-2.0 -I$DEPENDROOT/usr/lib/glib-2.0/include"

    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    if [ "$BUILD_OS" = "minoca" ]; then
        export LD_LIBRARY_PATH=$DEPENDROOT/usr/lib
    fi

    cp -Rp ${SOURCE_DIRECTORY}/* ${BUILD_DIRECTORY}
    sh ./configure $BUILD_LINE \
                   --host="$TARGET" \
                   --target="$TARGET" \
                   --prefix="/usr" \
                   --sysconfdir=/etc \
                   --disable-static \
                   --with-pic=yes \
                   $WITH_HARFBUZZ \
                   CFLAGS="$CFLAGS" \
                   LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

