##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the node utility.
##
## Author:
##
##     Evan Green 17-Aug-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

rm -rf "$PACKAGE_DIRECTORY"

##
## Create the libuv package.
##

mkdir -p "$PACKAGE_DIRECTORY/usr/lib"
mkdir -p "$PACKAGE_DIRECTORY/usr/include"
mkdir -p "$PACKAGE_DIRECTORY/CONTROL"
cp -pv "$BUILD_DIRECTORY/libuv/usr/lib/libuv.so.1" "$PACKAGE_DIRECTORY/usr/lib"
INCLUDES="uv.h uv-errno.h uv-threadpool.h uv-version.h uv-unix.h uv-minoca.h"
for i in $INCLUDES; do
    cp -pv "$BUILD_DIRECTORY/libuv/usr/include/$i" \
           "$PACKAGE_DIRECTORY/usr/include/$i"

done
chown -R root:root "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libuv
Depends: libgcc
Priority: optional
Version: 1.9.1
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://nodejs.org/dist/v4.5.0/node-v4.5.0.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Event abstraction library.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

##
## Create the Node package
##

rm -rf "$PACKAGE_DIRECTORY"
mkdir -p "$PACKAGE_DIRECTORY/CONTROL"
mkdir -p "$PACKAGE_DIRECTORY/usr/share"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/share/systemtap" "$PACKAGE_DIRECTORY/usr/share"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: nodejs
Depends: libopenssl, libuv, libgcc
Priority: optional
Version: 4.5.0
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://nodejs.org/dist/v4.5.0/node-v4.5.0.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Node.js programming environment.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

