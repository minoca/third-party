##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the Apache httpd server.
##
## Author:
##
##     Evan Green 20-Jun-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY"
cp -Rpv "$BUILD_DIRECTORY/etc" "$PACKAGE_DIRECTORY"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/var/log/httpd"
mkdir -p "$PACKAGE_DIRECTORY/var/run"
mkdir -p "$PACKAGE_DIRECTORY/var/www/html"

##
## Enable slotmem_shm_module and disable lbmethod_heartbeat_module in the
## default configuration.
##

hbmod='LoadModule lbmethod_heartbeat_module lib/apache/mod_lbmethod_heartbeat.so'
slotshm='LoadModule slotmem_shm_module lib/apache/mod_slotmem_shm.so'
sed -e "s|$hbmod|#$hbmod|" -e "s|#$slotshm|$slotshm|" \
    "$BUILD_DIRECTORY/etc/httpd/conf/httpd.conf" \
    >"$PACKAGE_DIRECTORY/etc/httpd/conf/httpd.conf"

##
## Create a basic index.html page.
##

cat >"$PACKAGE_DIRECTORY/var/www/html/index.html" <<_EOS
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Apache Works!</title>
  </head>
  <body>
    <h1>It works!</h1>
  </body>
</html>
_EOS

##
## Create the init script
##

mkdir -p "$PACKAGE_DIRECTORY/etc/init.d"
cat > "$PACKAGE_DIRECTORY/etc/init.d/apache" <<_EOS
#! /bin/sh
set -e

# /etc/init.d/apache: start and stop the Apache web server.

HTTPD=/usr/sbin/httpd
LOGDIR=/var/log/apache
PIDDIR=/var/run
PIDFILE="\$PIDDIR/apache.pid"

test -x \$HTTPD || exit 0
( \$HTTPD -V 2>&1 | grep -q Apache ) 2>/dev/null || exit 0

[ -f /etc/init.d/init-functions ] && . /etc/init.d/init-functions

umask 022
if test -f /etc/default/apache; then
    . /etc/default/apache
fi

[ -d "\$LOGDIR" ] || mkdir -p "\$LOGDIR"
[ -d "\$PIDDIR" ] || mkdir -p "\$PIDDIR"

export PATH="\${PATH:+\$PATH:}/usr/sbin:/sbin"

case "\$1" in
    start)
        log_daemon_msg "Starting Apache" || true
        if \$HTTPD -k start; then
            log_end_msg 0 || true

        else
            log_end_msg 1 || true

        fi
        ;;

    stop)
        log_daemon_msg "Stopping Apache" || true
        if \$HTTPD -k stop; then
            log_end_msg 0 || true

        else
            log_end_msg 1 || true
        fi
        ;;

    restart|graceful|graceful-stop)
        log_daemon_msg "Performing \$1 on Apache" || true
        if \$HTTPD -k "\$1"; then
            log_end_msg 0 || true

        else
            log_end_msg 1 || true
        fi
        ;;

    configtest)
        \$HTTPD -t
        ;;

    *)
        log_action_msg \
             "Usage: \$0 {start|stop|restart|graceful|graceful-stop|configtest}"

        exit 1
        ;;

esac

exit 0
_EOS
chmod +x "$PACKAGE_DIRECTORY/etc/init.d/apache"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: apache
Depends: libaprutil, libpcre, libgcc, libz, libopenssl, libxml2
Priority: optional
Version: 2.4.20
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://www-us.apache.org/dist//httpd/httpd-2.4.20.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Apache web server.
_EOS

cat > "$PACKAGE_DIRECTORY/CONTROL/conffiles" <<_EOS
/var/www/html/index.html
/etc/httpd/conf/httpd.conf
/etc/httpd/conf/extra/httpd-autoindex.conf
/etc/httpd/conf/extra/httpd-dav.conf
/etc/httpd/conf/extra/httpd-default.conf
/etc/httpd/conf/extra/httpd-info.conf
/etc/httpd/conf/extra/httpd-languages.conf
/etc/httpd/conf/extra/httpd-manual.conf
/etc/httpd/conf/extra/httpd-mpm.conf
/etc/httpd/conf/extra/httpd-ssl.conf
/etc/httpd/conf/extra/httpd-multilang-errordoc.conf
/etc/httpd/conf/extra/httpd-userdir.conf
/etc/httpd/conf/extra/httpd-vhosts.conf
/etc/httpd/conf/extra/proxy-html.conf
/etc/httpd/conf/mime.types
/etc/httpd/conf/magic
_EOS

cat > "$PACKAGE_DIRECTORY/CONTROL/prerm" <<"_EOS"
#! /bin/sh
if test "x$D" != "x"; then
    exit 1
else
    /etc/init.d/apache stop
    update-rc.d -f apache remove
    rm -f /etc/init.d/apache
fi
_EOS

chmod +x "$PACKAGE_DIRECTORY/CONTROL/prerm"

cat > "$PACKAGE_DIRECTORY/CONTROL/postinst" <<"_EOS"
#! /bin/sh

ROOTLINE=
CHROOTCMD=
if test "$PKG_ROOT" != "/"; then
    ROOTLINE="-R$PKG_ROOT"
    CHROOTCMD="chroot $PKG_ROOT -- "
fi

if test "x$D" != "x"; then
    exit 1

else
    useradd $ROOTLINE --system --home=/var/www --no-create-home \
        --shell=/bin/false www-data || true

    $CHROOTCMD /usr/sbin/update-rc.d apache defaults
fi
_EOS

chmod +x "$PACKAGE_DIRECTORY/CONTROL/postinst"

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

