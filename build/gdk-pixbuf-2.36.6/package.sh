##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the gdk-pixbuf library.
##
## Author:
##
##     Evan Green 19-Apr-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

##
## Avoid trailing slashes in the cp commands, as cp on Darwin copies the
## contents of the directory (rather than the directory itself) if there are
## trailing slashes on either the source or the destination.
##

rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/usr"
rm -f $BUILD_DIRECTORY/usr/lib/*.la
rm -f $BUILD_DIRECTORY/usr/lib/gdk-pixbuf-2.0/2.10.0/loaders/*.la
rm -rf $BUILD_DIRECTORY/usr/share/man
cp -Rpv "$BUILD_DIRECTORY/usr" "$PACKAGE_DIRECTORY"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: gdk-pixbuf
Depends: libglib, libjpeg-turbo, libpng, tiff, gettext, libx11, libgcc
Priority: optional
Version: 2.36.6
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://ftp.gnome.org/pub/gnome/sources/gdk-pixbuf/2.36/gdk-pixbuf-2.36.6.tar.xz
Installed-Size: $INSTALLED_SIZE
Description: Toolkit for image loading and pixel manipulation
_EOS

cat > "$PACKAGE_DIRECTORY/CONTROL/postinst" <<"_EOS"
#!/bin/sh
$PKG_ROOT/usr/bin/gdk-pixbuf-query-loaders > \
    $PKG_ROOT/usr/lib/gdk-pixbuf-2.0/2.10.0/loaders.cache

_EOS

chmod +x "$PACKAGE_DIRECTORY/CONTROL/postinst"
cat > "$PACKAGE_DIRECTORY/CONTROL/prerm" <<"_EOS"
#!/bin/sh
rm -f $PKG_ROOT/usr/lib/gdk-pixbuf-2.0/2.10.0/loaders.cache

_EOS
chmod +x "$PACKAGE_DIRECTORY/CONTROL/prerm"

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

