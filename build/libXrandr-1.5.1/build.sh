##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the libXrandr-1.5.1 package.
##
## Author:
##
##     Evan Green 22-Mar-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh
. ../x_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency libxau_1.0.8
    extract_dependency libxcb_1.12
    extract_dependency libx11_1.6.5
    extract_dependency xproto_7.0.31
    extract_dependency xextproto_7.3.0
    extract_dependency kbproto_1.0.7
    extract_dependency libxext_1.3.3
    extract_dependency libsm_1.2.2
    extract_dependency libice_1.0.9
    extract_dependency libxt_1.1.5
    extract_dependency libxmu_1.1.2
    extract_dependency libxpm_3.5.12
    extract_dependency libxfixes_5.0.3
    extract_dependency libxrender_0.9.10
    extract_dependency libz_1.2.11
    extract_dependency libfreetype_2.7.1
    extract_dependency xtrans_1.3.5
    extract_dependency fontsproto_2.1.3
    extract_dependency libfontenc_1.1.3
    extract_dependency expat_2.1.0
    extract_dependency inputproto_2.3.2
    extract_dependency xineramaproto_1.2.1
    extract_dependency randrproto_1.5.0

    sh ${SOURCE_DIRECTORY}/configure $XORG_CONFIG
    ;;

  build)
    $MAKE $PARALLEL_MAKE V=1
    $MAKE $PARALLEL_MAKE install V=1 DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

