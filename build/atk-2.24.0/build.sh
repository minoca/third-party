##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the atk package.
##
## Author:
##
##     Evan Green 18-Apr-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh

export PATH="$PATH:$DEPENDROOT/usr/bin"
export LD_LIBRARY_PATH=$DEPENDROOT/usr/lib

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency libiconv_1.14
    extract_dependency libglib_2.49.6
    extract_dependency gettext_0.19.8.1
    extract_dependency bison_3.0.2

    extract_dependency libpcre_8.39
    extract_dependency libffi_3.2.1

    export CFLAGS="$CFLAGS"
    export CPPFLAGS="-I$DEPENDROOT/usr/include \
 -I$DEPENDROOT/usr/include/glib-2.0 \
 -I$DEPENDROOT/usr/lib/glib-2.0/include"

    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     --disable-static \
                                     --disable-rpath \
                                     --sysconfdir=/etc \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

