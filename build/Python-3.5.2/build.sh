##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the Python package.
##
## Author:
##
##     Evan Green 15-Dec-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

NCURSES=libncurses_5.9
SQLITE=sqlite_3080500
OPENSSL=libopenssl_1.0.2h
READLINE=libreadline_6.3
ZLIB=libz_1.2.11
BZIP2=bzip2_1.0.6
EXPAT=expat_2.1.0

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    if test "x$BUILD_OS" != "xminoca"; then
        echo "Error: Python cannot be cross-compiled."
        exit 3
    fi

    ##
    ## Export CFLAGS and LDFLAGS so they're used when building extensions too.
    ## Python's build process seems to be less than ideal, as even though these
    ## includes/libs are used during build, setup.py still needs to be able to
    ## find things like zlib.h in a standard path before it will even attempt
    ## to build that extension.
    ##

    extract_dependency "$NCURSES"
    extract_dependency "$SQLITE"
    extract_dependency "$OPENSSL"
    extract_dependency "$READLINE"
    extract_dependency "$ZLIB"
    extract_dependency "$BZIP2"
    extract_dependency "$EXPAT"
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    ##
    ## Avoid hard links.
    ##

    export LN='ln -s'

    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure --disable-ipv6       \
                                     --enable-shared      \
                                     --with-system-expat  \
                                     --prefix="/usr"      \
                                     --sysconfdir="/etc"  \
                                     --localstatedir=/var \
                                     --build=$TARGET      \
                                     --host=$TARGET       \
                                     CFLAGS="$CFLAGS"     \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

