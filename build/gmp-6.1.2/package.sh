##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the GMP library.
##
## Author:
##
##     Evan Green 18-Jan-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"

##
## Libtool sucks.
##

rm -f $PACKAGE_DIRECTORY/usr/lib/*.la

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libgmp
Priority: optional
Version: 6.1.2
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://ftp.gnu.org/gnu/gmp/gmp-6.1.2.tar.xz
Installed-Size: $INSTALLED_SIZE
Description: GNU multiprecision library
 GMP is a free library for arbitrary precision arithmetic,
 operating on signed integers, rational numbers, and floating point numbers.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

